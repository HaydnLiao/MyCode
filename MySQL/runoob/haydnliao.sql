-- MySQL dump 10.14  Distrib 5.5.52-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: haydnliao
-- ------------------------------------------------------
-- Server version	5.5.52-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `apps`
--

DROP TABLE IF EXISTS `apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `url` varchar(100) NOT NULL,
  `country` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps`
--

LOCK TABLES `apps` WRITE;
/*!40000 ALTER TABLE `apps` DISABLE KEYS */;
INSERT INTO `apps` VALUES (1,'qq','im.qq.com','CN'),(2,'tb','www.taobao.com','CN');
/*!40000 ALTER TABLE `apps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `haydn_tb1`
--

DROP TABLE IF EXISTS `haydn_tb1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `haydn_tb1` (
  `haydn_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `haydn_title` varchar(100) NOT NULL,
  `haydn_author` varchar(40) NOT NULL,
  `submission_date` date DEFAULT NULL,
  PRIMARY KEY (`haydn_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `haydn_tb1`
--

LOCK TABLES `haydn_tb1` WRITE;
/*!40000 ALTER TABLE `haydn_tb1` DISABLE KEYS */;
INSERT INTO `haydn_tb1` VALUES (1,'Python','HaydnLiao','2017-07-03'),(3,'Office','MayLin','2017-07-01');
/*!40000 ALTER TABLE `haydn_tb1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(10) NOT NULL DEFAULT '',
  `date` datetime NOT NULL,
  `singin` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'logon',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES (1,'Ming','2016-04-22 15:25:33',1),(2,'Wang','2016-04-20 15:25:47',3),(3,'Li','2016-04-19 15:26:02',2),(4,'Wang','2016-04-07 15:26:14',4),(5,'Ming','2016-04-11 15:26:40',4),(6,'Ming','2016-04-04 15:26:54',2);
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tcount_clone`
--

DROP TABLE IF EXISTS `tcount_clone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tcount_clone` (
  `author` varchar(255) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`author`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tcount_clone`
--

LOCK TABLES `tcount_clone` WRITE;
/*!40000 ALTER TABLE `tcount_clone` DISABLE KEYS */;
INSERT INTO `tcount_clone` VALUES ('Baidu',10),('Google',20),('Taobao',22);
/*!40000 ALTER TABLE `tcount_clone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tcount_tb1`
--

DROP TABLE IF EXISTS `tcount_tb1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tcount_tb1` (
  `author` varchar(255) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`author`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tcount_tb1`
--

LOCK TABLES `tcount_tb1` WRITE;
/*!40000 ALTER TABLE `tcount_tb1` DISABLE KEYS */;
INSERT INTO `tcount_tb1` VALUES ('Baidu',10),('Google',20),('Taobao',22);
/*!40000 ALTER TABLE `tcount_tb1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_tb`
--

DROP TABLE IF EXISTS `transaction_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_tb` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `num` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_tb`
--

LOCK TABLES `transaction_tb` WRITE;
/*!40000 ALTER TABLE `transaction_tb` DISABLE KEYS */;
INSERT INTO `transaction_tb` VALUES (1,5),(2,8);
/*!40000 ALTER TABLE `transaction_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `websites`
--

DROP TABLE IF EXISTS `websites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `websites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `url` varchar(100) CHARACTER SET utf8 NOT NULL,
  `country` varchar(5) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `websites`
--

LOCK TABLES `websites` WRITE;
/*!40000 ALTER TABLE `websites` DISABLE KEYS */;
INSERT INTO `websites` VALUES (1,'StackOverflow','stackoverflow.com','IND'),(2,'Facebook','www.facebook.com','USA'),(4,'TaoBao','www.taobao.com','CN'),(5,'Google','www.google.com','USA'),(6,'Google','google.com.hk','CN'),(7,'TaoBao','taobao.com','CN');
/*!40000 ALTER TABLE `websites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `websites_clone`
--

DROP TABLE IF EXISTS `websites_clone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `websites_clone` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `url` varchar(100) CHARACTER SET utf8 NOT NULL,
  `country` varchar(5) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `websites_clone`
--

LOCK TABLES `websites_clone` WRITE;
/*!40000 ALTER TABLE `websites_clone` DISABLE KEYS */;
INSERT INTO `websites_clone` VALUES (1,'StackOverflow','stackoverflow.com','IND'),(2,'Facebook','www.facebook.com','USA'),(3,'TaoBao','www.taobao.com','CN'),(4,'Google','www.google.com','USA');
/*!40000 ALTER TABLE `websites_clone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `websites_in`
--

DROP TABLE IF EXISTS `websites_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `websites_in` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `url` varchar(100) NOT NULL,
  `country` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `websites_in`
--

LOCK TABLES `websites_in` WRITE;
/*!40000 ALTER TABLE `websites_in` DISABLE KEYS */;
INSERT INTO `websites_in` VALUES (1,'StackOverflow','stackoverflow.com','IND'),(2,'Facebook','www.facebook.com','USA'),(4,'TaoBao','www.taobao.com','CN'),(5,'Google','www.google.com','USA'),(6,'Google','google.com.hk','CN'),(7,'TaoBao','taobao.com','CN');
/*!40000 ALTER TABLE `websites_in` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-05 23:32:12
