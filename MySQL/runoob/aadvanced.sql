
-- 临时表 (MySQL v3.23 以上)
-- 只在当前连接可见,当关闭连接时会自动删除并释放所有空间.
-- 使用 SHOW TABLES 命令显示数据表列表时, 临时表无显示.
CREATE TEMPORARY TABLE table_name (column_name column_type);
MariaDB [haydnliao]> CREATE TEMPORARY TABLE temp_tb(
	-> name VARCHAR(50) NOT NULL
	-> , totle_sales DECIMAL(12, 2) NOT NULL DEFAULT 0.00
	-> , avg_units_price DECIMAL(7, 2) NOT NULL DEFAULT 0.00
	-> , totle_units_sold INT UNSIGNED NOT NULL DEFAULT 0
	-> );
Query OK, 0 rows affected (0.06 sec)

MariaDB [haydnliao]> SHOW TABLES;
+---------------------+
| Tables_in_haydnliao |
+---------------------+
| apps                |
| haydn_tb1           |
| login               |
| tcount_tb1          |
| transaction_tb      |
| websites            |
+---------------------+
6 rows in set (0.00 sec)

MariaDB [haydnliao]> INSERT INTO temp_tb VALUES ('cucumber', 100.25, 90, 2);
Query OK, 1 row affected (0.00 sec)

MariaDB [haydnliao]> SELECT * FROM temp_tb;
+----------+-------------+-----------------+------------------+
| name     | totle_sales | avg_units_price | totle_units_sold |
+----------+-------------+-----------------+------------------+
| cucumber |      100.25 |           90.00 |                2 |
+----------+-------------+-----------------+------------------+
1 row in set (0.00 sec)

-- 断开连接重新连接表已经被销毁
MariaDB [haydnliao]> SELECT * FROM temp_tb;
ERROR 1146 (42S02): Table 'haydnliao.temp_tb' doesn''t exist

-- 复制表
/*
使用 SHOW CREATE TABLE 命令获取创建数据表(CREATE TABLE) 语句,该语句包含了原数据表的结构,索引等.
复制 SHOW 命令显示的SQL语句,修改数据表名,并执行SQL语句,将完全的复制数据表结构.
复制表的内容可以使用 INSERT INTO ... SELECT 语句来实现.
*/
-- 步骤一：获取数据表的完整结构. SHOW CREATE TABLE 
MariaDB [haydnliao]> SHOW CREATE TABLE tcount_tb1 \G;
*************************** 1. row ***************************
	   Table: tcount_tb1
Create Table: CREATE TABLE `tcount_tb1` (
  `author` varchar(255) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`author`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
1 row in set (0.00 sec)

ERROR: No query specified
-- 步骤二：修改SQL语句的数据表名,并执行SQL语句,创建新的克隆表 clone_tbl.
MariaDB [haydnliao]> CREATE TABLE tcount_clone(
	->   `author` varchar(255) NOT NULL DEFAULT '',
	->   `count` int(11) NOT NULL DEFAULT '0',
	->   PRIMARY KEY (`author`)
	-> ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
Query OK, 0 rows affected (0.00 sec)
-- 步骤三：拷贝数据表的数据可以使用 INSERT INTO ... SELECT 语句来实现.
MariaDB [haydnliao]> INSERT INTO tcount_clone
	-> SELECT * FROM tcount_tb1;
Query OK, 3 rows affected (0.41 sec)
Records: 3  Duplicates: 0  Warnings: 0

MariaDB [haydnliao]> SELECT * FROM tcount_tb1;
+--------+-------+
| author | count |
+--------+-------+
| Baidu  |    10 |
| Google |    20 |
| Taobao |    22 |
+--------+-------+
3 rows in set (0.00 sec)

MariaDB [haydnliao]> SELECT * FROM tcount_clone;
+--------+-------+
| author | count |
+--------+-------+
| Baidu  |    10 |
| Google |    20 |
| Taobao |    22 |
+--------+-------+
3 rows in set (0.00 sec)

-- 元数据
/*
查询结果信息： SELECT, UPDATE 或 DELETE 语句影响的记录数.
数据库和数据表的信息： 包含了数据库及数据表的结构信息.
MySQL服务器信息： 包含了数据库服务器的当前状态,版本号等.
*/
-- 获取服务器元数据
/*
以下命令语句可以在 MySQL 的命令提示符使用,也可以在脚本中 使用,如PHP脚本.
SELECT VERSION( )	服务器版本信息
SELECT DATABASE( )	当前数据库名 (或者返回空)
SELECT USER( )		当前用户名
SHOW STATUS			服务器状态
SHOW VARIABLES		服务器配置变量
*/
MariaDB [haydnliao]> SELECT VERSION();
+----------------+
| VERSION()      |
+----------------+
| 5.5.52-MariaDB |
+----------------+
1 row in set (0.00 sec)

MariaDB [haydnliao]> SELECT DATABASE();
+------------+
| DATABASE() |
+------------+
| haydnliao  |
+------------+
1 row in set (0.00 sec)

MariaDB [haydnliao]> SELECT USER();
+----------------+
| USER()         |
+----------------+
| root@localhost |
+----------------+
1 row in set (0.00 sec)

-- 序列 AUTO_INCREMENT
/*
序列是一组整数：1, 2, 3, ...,一张数据表只能有一个字段自增且为主键.
*/
-- 获取 AUTO_INCREMENT 的值(四种)
-- SELECT LAST_INSERT_ID(); 获取最后的插入表中的自增列的值.
-- SELECT @@INDENTITY; 全局变量@@IDENTITY表示最近一次
-- 向具有identity属性（auto_increment）的表INSERT数据时对应的自增列的值.
-- SHOW TABLE STATUS WHERE name='tbl_name' \G;
MariaDB [haydnliao]> SHOW TABLE STATUS WHERE name='websites' \G;
*************************** 1. row ***************************
           Name: websites
         Engine: InnoDB
        Version: 10
     Row_format: Compact
           Rows: 4
 Avg_row_length: 4096
    Data_length: 16384
Max_data_length: 0
   Index_length: 0
      Data_free: 7340032
 Auto_increment: 5
    Create_time: 2017-07-04 06:04:02
    Update_time: NULL
     Check_time: NULL
      Collation: utf8_general_ci
       Checksum: NULL
 Create_options: 
        Comment: 
1 row in set (0.00 sec)

ERROR: No query specified
-- SELECT table_name, AUTO_INCREMENT FROM information_schema.tables WHERE table_name='tbl_name';
MariaDB [haydnliao]> SELECT table_name, AUTO_INCREMENT FROM information_schema.tables WHERE table_name='websites';
+------------+----------------+
| table_name | AUTO_INCREMENT |
+------------+----------------+
| websites   |              5 |
+------------+----------------+
1 row in set (0.00 sec)

-- 重置序列
-- 直接重置 AUTO_INCREMENT
ALTER TABLE tbl_name AUTO_INCREMENT = 1;
-- 通过删除自增的列然后重新添加
ALTER TABLE tbl_name DROP col_name;
ALTER TABLE tbl_name ADD col_name col_type FIRST, ADD PRIMARY KEY (col_name);
-- 删除记录初始化序列 TRUNCATE
TRUNCATE TABLE tbl_name;

-- 处理重复数据
-- 设置双主键模式来设置数据的唯一性.
ALTER TABLE tbl_name ADD PRIMARY KEY (col_name1, col_name2);
-- 添加一个UNIQUE索引来设置数据的唯一性.
CREATE UNIQUE INDEX indexName ON tbl_name(col_name(length)); 
-- INSERT IGNORE INTO 如果数据库没有数据,就插入新的数据,如果有数据的话就跳过这条数据.
INSERT IGNORE INTO tbl_name (col_name1, col_name2, ...) VALUES (value1, value2, ...);
-- REPLACE INTO 如果存在 PRIMARY 或 UNIQUE 相同的记录,则先删除掉,再插入新记录.
REPLACE INTO tbl_name (col_name1, col_name2, ...) VALUES (value1, value2, ...);

-- 统计重复数据
SELECT rep_col_name, COUNT(*) AS rep_times FROM tbl_name
GROUP BY rep_col_name
HAVING rep_times > 1

-- 过滤重复数据
-- 需要读取不重复的数据可以在 SELECT 语句中使用 DISTINCT 关键字.
SELECT DISTINCT rep_col_name FROM tbl_name;
-- 使用 GROUP BY.
SELECT * FROM tbl_name GROUP BY rep_col_name;

-- 删除重复数据
-- 复制表结构保存不重复的数据.
SHOW CREATE TABLE tbl_name \G;
CREATE TABLE clone_tbl_name(...);
INSERT INTO clone_tbl_name SELECT * FROM tbl_name GROUP BY rep_col_name;
DROP TABLE tbl_name;
ALTER TABLE clone_tbl_name RENAME TO tbl_name;
-- 直接操作数据表删除重复数据.
DELETE FROM tbl_name WHERE col_name NOT IN
(SELECT minid FROM (SELECT MIN(col_name) minid FROM tbl_name GROUP BY rep_col_name) min_tb);

-- SQL 注入
/*
通过把SQL命令插入到Web表单递交或输入域名或页面请求的查询字符串中,
最终达到欺骗服务器执行恶意的SQL命令.
*/
/*
1.永远不要信任用户的输入.对用户的输入进行校验,可以通过正则表达式,或限制长度;对单引号和双"-"进行转换等.
2.永远不要使用动态拼装sql,可以使用参数化的sql或者直接使用存储过程进行数据查询存取.
3.永远不要使用管理员权限的数据库连接,为每个应用使用单独的权限有限的数据库连接.
4.不要把机密信息直接存放,加密,或者hash掉密码和敏感的信息.
5.应用的异常信息应该给出尽可能少的提示,最好使用自定义的错误信息对原始错误信息进行包装.
6.sql注入的检测方法一般采取辅助软件或网站平台来检测.软件一般采用sql注入检测工具jsky.
网站平台就有亿思网站安全平台检测工具,MDCSOFT SCAN等.采用MDCSOFT-IPS可以有效的防御SQL注入,XSS攻击等.
*/

-- 导出数据表数据
-- 使用 SELECT ... INTO OUTFILE 语句导出数据
MariaDB [haydnliao]> SELECT * FROM websites INTO OUTFILE '~/websites_db.txt';
Query OK, 6 rows affected (0.05 sec)
/*
导出失败一般是目录权限不足,或者 selinux 问题.
在UNIX中,该文件被创建后是可读的,权限由MySQL服务器所拥有.
这意味着,虽然可以读取该文件,但可能无法将其删除.
*/
-- 可以通过命令选项来设置数据输出的指定格式,以下实例为导出 CSV 格式.
SELECT * FROM tbl_name INTO OUTFILE 'file_path'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n';
/*
FIELDS TERMINATED BY ','        字段间分割符
[OPTIONALLY] ENCLOSED BY '"'    字段包围符,对数值型无效
LINES TERMINATED BY '\r\n'      行分割符,回车换行
*/
MariaDB [haydnliao]> SELECT * FROM websites INTO OUTFILE '/websites_db.txt';
ERROR 1 (HY000): Can''t create/write to file '/websites_db.txt' (Errcode: 13)
MariaDB [haydnliao]> SELECT * FROM websites INTO OUTFILE '~/websites_db.txt'
    -> FIELDS TERMINATED BY ','
    -> ENCLOSED BY '"'
    -> LINES TERMINATED BY '\r\n';
Query OK, 6 rows affected (0.00 sec)

-- 读取数据表数据
-- 使用 LOAD DATA INFILE 将文件读取数据表（表要存在）
/*
如果指定LOCAL关键词,则从客户主机上按路径读取文件.
如果没有指定,则在服务器上按路径读取文件.
*/
LOAD DATA INFILE 'file_path' INTO TABLE tbl_name
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n';

MariaDB [haydnliao]> LOAD DATA INFILE '~/websites_db.txt' INTO TABLE websites_in
    -> FIELDS TERMINATED BY ','
    -> ENCLOSED BY '"'
    -> LINES TERMINATED BY '\r\n';
Query OK, 6 rows affected (0.00 sec)
Records: 6  Deleted: 0  Skipped: 0  Warnings: 0
/*
默认情况下是按照数据文件中列的顺序插入数据的.
如果数据文件中的列与插入表中的列不一致,则需要指定列的顺序.
*/
LOAD DATA LOCAL INFILE 'file_path' INTO TABLE tbl_name (col_name1, col_name2, ...);
-- 使用 mysqlimport 导入数据
-- 文件名应为 表名.txt
mysqlimport -u root -p db_name file_path
[root@localhost mysql]# mysqlimport -u root -p dumptest  /mnt/hgfs/win-MySQL/runoob/websites.txt 
Enter password: 
dumptest.websites: Records: 6  Deleted: 0  Skipped: 0  Warnings: 24
/*
--fields-terminated-by=":"          字段间分割符,默认为 TAB.
--fields-enclosed-by="""            字段包围符,默认为 无.
--lines-terminated-by="\r\n"        行分隔符,默认为 LF.
--columns=col_name1, col_name2, ... 设置列的顺序.
-d 或 --delete   新数据导入数据表中之前删除数据数据表中的所有信息.
-f 或 --force    不管是否遇到错误,强制继续插入数据.
-i 或 --ignore   跳过或者忽略有相同唯一关键字的行.
-l 或 --lock     数据插入前锁住表,防止更新数据库时用户的查询和更新受到影响.
-r 或 --replace  将替换表中有相同唯一关键字的记录.
*/

-- 导出表作为原始数据 mysqldump
/*
mysqldump是mysql用于转存储数据库的实用程序.
它主要产生一个SQL脚本,其中包含从头重新创建数据库所必需的命令CREATE TABLE INSERT等.
使用mysqldump导出数据使用 --tab 选项来指定导出文件指定的目录,该目标必须是可写的.
*/
-- 备份一个数据表
mysqldump -u root -p db_name tbl_name > file_path
[root@localhost haydnliao]# mysqldump -u root -p haydnliao websites > /mnt/hgfs/win-MySQL/runoob/haydnliao_websites.sql
Enter password: 
-- 备份整个数据库
mysqldump -u root -p db_name > file_path
[root@localhost haydnliao]# mysqldump -u root -p haydnliao > /mnt/hgfs/win-MySQL/runoob/haydnliao.sql
Enter password: 
-- 备份所有数据库
mysqldump -u root -p --all-databases > file_path
[root@localhost haydnliao]# mysqldump -u root -p --all-databases >  /mnt/hgfs/win-MySQL/runoob/all_databases.sql
Enter password: 

-- 导入一个表或整个数据库 mysql
mysql -u root -p db_name < file_path
[root@localhost haydnliao]# mysql -u root -p dumptest < /mnt/hgfs/win-MySQL/runoob/haydnliao_websites.sql 
Enter password: 
-- 导入所有数据库 mysql
mysql -u root -p < file_path
[root@localhost haydnliao]# mysql -u root -p  < /mnt/hgfs/win-MySQL/runoob/all_databases.sql
Enter password: 
-- 将数据库导入远程服务器 mysqldump | mysql
mysqldump -u root -p db_name | mysql -h other_host db_name
-h produce HTML output


