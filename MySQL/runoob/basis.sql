
-- 使用 mysqladmin 创建数据库
-- 创建数据库HAYDNLIAO
[root@localhost ~]# mysqladmin -u root -p create HAYDNLIAO
Enter password: 
MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| HAYDNLIAO          |
| mysql              |
| performance_schema |
| test               |
+--------------------+
5 rows in set (0.00 sec)

-- 使用 mysqladmin 删除数据库
-- 删除数据库HAYDNLIAO
[root@localhost ~]# mysqladmin -u root -p drop HAYDNLIAO
Enter password: 
Dropping the database is potentially a very bad thing to do.
Any data stored in the database will be destroyed.

Do you really want to drop the 'HAYDNLIAO' database [y/N] y
Database "HAYDNLIAO" dropped

-- MySQL 数据类型
-- 大致可以分为三类：数值、日期/时间和字符串(字符)类型。

-- 数值类型
/*
TINYINT			1 Byte		小整数值
SMALLINT		2 Byt		大整数值
MEDIUMINT		3 Byte		大整数值
INT / INTEGER	4 Byte		大整数值
BIGINT			8 Byte		极大整数值
FLOAT			4 Byte		单精度浮点数值
DOUBLE			8 Byte		双精度浮点数值
DECIMAL			对DECIMAL(M,D) ，如果M>D，为M+2否则为D+2		小数值
*/
-- 日期和时间类型
/*
YEAR		1 Byte	1901/2155								YYYY				年份值
DATE		3 Byte	1000-01-01/9999-12-31					YYYY-MM-DD			日期值
TIME		3 Byte	'-838:59:59'/'838:59:59'				HH:MM:SS			时间值或持续时间
TIMESTAMP	4 Byte	1970-01-01 00:00:00/2037 a certain time	YYYYMMDD HHMMSS		混合日期和时间值，时间戳
DATETIME	8 Byte	1000-01-01 00:00:00/9999-12-31 23:59:59	YYYY-MM-DD HH:MM:SS	混合日期和时间值
*/
-- 字符串类型
/*
CHAR		0-255 Byte				定长字符串
VARCHAR		0-65535 Byte			变长字符串
TINYBLOB	0-255 Byte				不超过 255 个字符的二进制字符串
TINYTEXT	0-255 Byte				短文本字符串
BLOB		0-65 535Byte			二进制形式的长文本数据
TEXT		0-65 535Byte			长文本数据
MEDIUMBLOB	0-16 777 215 Byte		二进制形式的中等长度文本数据
MEDIUMTEXT	0-16 777 215 Byte		中等长度文本数据
LONGBLOB	0-4 294 967 295 Byte	二进制形式的极大文本数据
LONGTEXT	0-4 294 967 295 Byte	极大文本数据
*/

-- 创建数据表：表名，表字段名，字段定义 CREATE TABLE
-- CREATE TABLE table_name (column_name column_type);
MariaDB [(none)]> use haydnliao
Database changed
MariaDB [haydnliao]> CREATE TABLE IF NOT EXISTS haydn_tb1(
    -> haydn_id INT UNSIGNED AUTO_INCREMENT,
    -> haydn_title VARCHAR(100) NOT NULL,
    -> haydn_author VARCHAR(40) NOT NULL,
    -> submission_date DATE,
    -> PRIMARY KEY (haydn_id)
    -> )ENGINE=InnoDB DEFAULT CHARSET=utf8;
Query OK, 0 rows affected (0.01 sec)
MariaDB [haydnliao]> SHOW TABLES;
+---------------------+
| Tables_in_haydnliao |
+---------------------+
| haydn_tb1           |
+---------------------+
1 row in set (0.00 sec)
MariaDB [haydnliao]> SHOW COLUMNS FROM haydn_tb1;
+-----------------+------------------+------+-----+---------+----------------+
| Field           | Type             | Null | Key | Default | Extra          |
+-----------------+------------------+------+-----+---------+----------------+
| haydn_id        | int(10) unsigned | NO   | PRI | NULL    | auto_increment |
| haydn_title     | varchar(100)     | NO   |     | NULL    |                |
| haydn_author    | varchar(40)      | NO   |     | NULL    |                |
| submission_date | date             | YES  |     | NULL    |                |
+-----------------+------------------+------+-----+---------+----------------+
4 rows in set (0.01 sec)

-- 删除数据表 DROP TABLE
-- DROP TABLE table_name;
MariaDB [haydnliao]> DROP TABLE haydn_tb2;
Query OK, 0 rows affected (0.01 sec)

-- 插入数据 INSERT
-- INSERT INTO table_name ( field1, field2,...fieldN )
-- 				VALUES ( value1, value2,...valueN );
MariaDB [haydnliao]> INSERT INTO haydn_tb1
    -> (haydn_title, haydn_author, submission_date)
    -> VALUES
    -> ('Python', 'HaydnLiao', NOW()),
    -> ('MySQL', 'HaydnLiao', NOW());
Query OK, 2 rows affected, 2 warnings (0.00 sec)
Records: 2  Duplicates: 0  Warnings: 2
MariaDB [haydnliao]> INSERT INTO haydn_tb1
    -> (haydn_title, haydn_author, submission_date)
    -> VALUES
    -> ('C', 'MayLin', NOW());
Query OK, 1 row affected, 1 warning (0.01 sec)

-- 查询数据 SELECT
/*
SELECT column_name,column_name
FROM table_name
[WHERE Clause]
[OFFSET M ][LIMIT N]
查询语句中可以使用一个或者多个表，表之间使用逗号(,)分隔，并使用WHERE语句来设定查询条件。
SELECT 命令可以读取一条或者多条记录。
你可以使用星号（*）来代替其他字段，SELECT语句会返回表的所有字段数据。
你可以使用 WHERE 语句来包含任何条件。
你可以通过OFFSET指定SELECT语句开始查询的数据偏移量。默认情况下偏移量为0。
你可以使用 LIMIT 属性来设定返回的记录数。
*/
MariaDB [haydnliao]> select * from haydn_tb1;
+----------+-------------+--------------+-----------------+
| haydn_id | haydn_title | haydn_author | submission_date |
+----------+-------------+--------------+-----------------+
|        1 | Python      | HaydnLiao    | 2017-07-03      |
|        2 | MySQL       | HaydnLiao    | 2017-07-03      |
|        3 | C           | MayLin       | 2017-07-03      |
+----------+-------------+--------------+-----------------+
3 rows in set (0.00 sec)

-- WHERE 子句
-- WHERE condition1 [AND [OR]] condition2.....
/*
可以使用 AND 或者 OR 指定一个或多个条件。
可以运用于 SQL 的 DELETE 或者 UPDATE 命令。
类似于程序语言中的 if 条件，根据 MySQL 表中的字段值来读取指定的数据。
操作符: = <>(不等于) !=(不等于) > < >= <=
*/
MariaDB [haydnliao]> SELECT * FROM haydn_tb1
    -> WHERE haydn_author != 'HaydnLiao';
+----------+-------------+--------------+-----------------+
| haydn_id | haydn_title | haydn_author | submission_date |
+----------+-------------+--------------+-----------------+
|        3 | C           | MayLin       | 2017-07-03      |
+----------+-------------+--------------+-----------------+
1 row in set (0.00 sec)

-- 修改或更新数据 UPDATE
-- UPDATE table_name SET field1=new-value1, field2=new-value2
-- [WHERE Clause]
/*
可以同时更新一个或多个字段。
可以在 WHERE 子句中指定任何条件。
可以在一个单独表中同时更新数据。
*/
MariaDB [haydnliao]> UPDATE haydn_tb1 SET haydn_title='Office',submission_date='2017-07-01'
    -> WHERE haydn_author='MayLin';
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [haydnliao]> SELECT * FROM haydn_tb1;
+----------+-------------+--------------+-----------------+
| haydn_id | haydn_title | haydn_author | submission_date |
+----------+-------------+--------------+-----------------+
|        1 | Python      | HaydnLiao    | 2017-07-03      |
|        2 | MySQL       | HaydnLiao    | 2017-07-03      |
|        3 | Office      | MayLin       | 2017-07-01      |
+----------+-------------+--------------+-----------------+
3 rows in set (0.00 sec)

-- 删除记录 DELETE
-- DELETE FROM table_name [WHERE Clause]
-- 如果没有指定 WHERE 子句，MySQL 表中的所有记录将被删除。
MariaDB [haydnliao]> DELETE FROM haydn_tb1 WHERE haydn_id=2;
Query OK, 1 row affected (0.01 sec)

MariaDB [haydnliao]> SELECT * FROM haydn_tb1;
+----------+-------------+--------------+-----------------+
| haydn_id | haydn_title | haydn_author | submission_date |
+----------+-------------+--------------+-----------------+
|        1 | Python      | HaydnLiao    | 2017-07-03      |
|        3 | Office      | MayLin       | 2017-07-01      |
+----------+-------------+--------------+-----------------+
2 rows in set (0.00 sec)

-- LIKE 子句
-- SELECT field1, field2,...fieldN 
-- FROM table_name
-- WHERE field1 LIKE condition1 [AND [OR]] filed2 = 'somevalue'
/*
LIKE 子句中使用百分号 % 来表示任意字符。
如果没有使用百分号 %, LIKE 子句与等号 = 的效果是一样的。
*/
MariaDB [haydnliao]> SELECT * FROM haydn_tb1 
    -> WHERE haydn_author LIKE '%Lin';
+----------+-------------+--------------+-----------------+
| haydn_id | haydn_title | haydn_author | submission_date |
+----------+-------------+--------------+-----------------+
|        3 | Office      | MayLin       | 2017-07-01      |
+----------+-------------+--------------+-----------------+
1 row in set (0.00 sec)

-- UNION 操作符
SELECT expression1, expression2, ... expression_n
/*
用于连接两个以上的 SELECT 语句的结果组合到一个结果集合中。
多个 SELECT 语句会删除重复的数据。
*/
-- FROM tables
-- [WHERE conditions]
-- UNION [ALL | DISTINCT]
-- SELECT expression1, expression2, ... expression_n
-- FROM tables
-- [WHERE conditions];
/*
DISTINCT: 可选，删除结果集中重复的数据。
默认情况下 UNION 操作符已经删除了重复数据，所以 DISTINCT 修饰符对结果没影响。
ALL: 可选，返回所有结果集，包含重复数据。
*/
MariaDB [haydnliao]> CREATE TABLE IF NOT EXISTS websites(
    -> id INT UNSIGNED AUTO_INCREMENT,
    -> name VARCHAR(20) NOT NULL,
    -> url VARCHAR(100) NOT NULL,
    -> country VARCHAR(5) NOT NULL,
    -> PRIMARY KEY (id)
    -> )ENGINE=InnoDB DEFAULT CHARSET=utf8;
Query OK, 0 rows affected (0.01 sec)

MariaDB [haydnliao]> INSERT INTO websites
    -> (name, url, country)
    -> VALUES
    -> ('Google', 'www.google.com', 'USA'), 
    -> ('TaoBao', 'www.taobao.com', 'CN'),
    -> ('Facebook', 'www.facebook.com', 'USA'),
    -> ('StackOverflow', 'stackoverflow.com', 'IND');
Query OK, 4 rows affected (0.01 sec)
Records: 4  Duplicates: 0  Warnings: 0

MariaDB [haydnliao]> SELECT * FROM websites;
+----+---------------+-------------------+---------+
| id | name          | url               | country |
+----+---------------+-------------------+---------+
|  1 | Google        | www.google.com    | USA     |
|  2 | TaoBao        | www.taobao.com    | CN      |
|  3 | Facebook      | www.facebook.com  | USA     |
|  4 | StackOverflow | stackoverflow.com | IND     |
+----+---------------+-------------------+---------+
4 rows in set (0.00 sec)

MariaDB [haydnliao]> CREATE TABLE IF NOT EXISTS apps(
    -> id INT UNSIGNED AUTO_INCREMENT,
    -> name VARCHAR(20) NOT NULL,
    -> url VARCHAR(100) NOT NULL,
    -> country VARCHAR(5) NOT NULL,
    -> PRIMARY KEY (id)
    -> )ENGINE=Inoob DEFAULT CHARSET=utf8;
Query OK, 0 rows affected, 2 warnings (0.00 sec)

MariaDB [haydnliao]> INSERT INTO apps
    -> 
    -> (name, url, country)
    -> VALUES
    -> ('qq', 'im.qq.com', 'CN'),
    -> ('tb', 'www.taobao.com', 'CN');
Query OK, 2 rows affected (0.00 sec)
Records: 2  Duplicates: 0  Warnings: 0

MariaDB [haydnliao]> SELECT * FROM apps;
+----+------+----------------+---------+
| id | name | url            | country |
+----+------+----------------+---------+
|  1 | qq   | im.qq.com      | CN      |
|  2 | tb   | www.taobao.com | CN      |
+----+------+----------------+---------+
2 rows in set (0.00 sec)

MariaDB [haydnliao]> SELECT url FROM websites
    -> UNION 
    -> SELECT url FROM apps
    -> ORDER BY url;
+-------------------+
| url               |
+-------------------+
| im.qq.com         |
| stackoverflow.com |
| www.facebook.com  |
| www.google.com    |
| www.taobao.com    |
+-------------------+
5 rows in set (0.00 sec)

MariaDB [haydnliao]> SELECT name, url FROM websites
    -> WHERE country='CN'
    -> UNION
    -> SELECT name, url FROM apps
    -> WHERE country='CN'
    -> ORDER BY name;
+--------+----------------+
| name   | url            |
+--------+----------------+
| qq     | im.qq.com      |
| TaoBao | www.taobao.com |
| tb     | www.taobao.com |
+--------+----------------+
3 rows in set (0.01 sec)

-- 排序 ORDER BY
-- SELECT field1, field2,...fieldN FROM table_name1, table_name2...
-- ORDER BY field1, [field2...] [ASC [DESC]]
/*
可以设定多个字段来排序。
可以使用 ASC 或 DESC 关键字来设置查询结果是按升序或降序排列。
默认情况下按升序排列。
*/
MariaDB [haydnliao]> SELECT * FROM haydn_tb1
    -> ORDER BY submission_date;
+----------+-------------+--------------+-----------------+
| haydn_id | haydn_title | haydn_author | submission_date |
+----------+-------------+--------------+-----------------+
|        3 | Office      | MayLin       | 2017-07-01      |
|        1 | Python      | HaydnLiao    | 2017-07-03      |
+----------+-------------+--------------+-----------------+
2 rows in set (0.00 sec)

-- 分组 GROUP BY
/*
GROUP BY 语句根据一个或多个列对结果集进行分组。
在分组的列上我们可以使用 COUNT, SUM, AVG等函数。
*/
-- SELECT column_name, function(column_name)
-- FROM table_name
-- WHERE column_name operator value
-- GROUP BY column_name;
MariaDB [haydnliao]> CREATE TABLE IF NOT EXISTS login(
    -> id INT UNSIGNED AUTO_INCREMENT,
    -> name CHAR(10) NOT NULL DEFAULT'',
    -> date DATETIME NOT NULL,
    -> singin TINYINT(4) NOT NULL DEFAULT 0 COMMENT 'logon',
    -> PRIMARY KEY (id)
    -> )ENGINE=InnoDB DEFAULT CHARSET=utf8;

MariaDB [haydnliao]> INSERT INTO login
    -> (name, date, singin)
    -> VALUES
    -> ('Ming', '2016-04-22 15:25:33', '1'), 
    -> ('Wang', '2016-04-20 15:25:47', '3'),
    -> ('Li', '2016-04-19 15:26:02', '2'),
    -> ('Wang', '2016-04-07 15:26:14', '4'),
    -> ('Ming', '2016-04-11 15:26:40', '4'),
    -> ('Ming', '2016-04-04 15:26:54', '2');
Query OK, 6 rows affected (0.00 sec)
Records: 6  Duplicates: 0  Warnings: 0

MariaDB [haydnliao]> SELECT * FROM login;
+----+------+---------------------+--------+
| id | name | date                | singin |
+----+------+---------------------+--------+
|  1 | Ming | 2016-04-22 15:25:33 |      1 |
|  2 | Wang | 2016-04-20 15:25:47 |      3 |
|  3 | Li   | 2016-04-19 15:26:02 |      2 |
|  4 | Wang | 2016-04-07 15:26:14 |      4 |
|  5 | Ming | 2016-04-11 15:26:40 |      4 |
|  6 | Ming | 2016-04-04 15:26:54 |      2 |
+----+------+---------------------+--------+
6 rows in set (0.00 sec)

MariaDB [haydnliao]> SELECT name, COUNT(*) as count FROM login
    -> GROUP BY name;
+------+-------+
| name | count |
+------+-------+
| Li   |     1 |
| Ming |     3 |
| Wang |     2 |
+------+-------+
3 rows in set (0.00 sec)

MariaDB [haydnliao]> SELECT name, SUM(singin) AS singin, COUNT(*) AS count
    -> FROM login
    -> GROUP BY name;
+------+--------+-------+
| name | singin | count |
+------+--------+-------+
| Li   |      2 |     1 |
| Ming |      7 |     3 |
| Wang |      7 |     2 |
+------+--------+-------+
3 rows in set (0.00 sec)

-- 使用 WITH ROLLUP
-- WITH ROLLUP 可以实现在分组统计数据基础上再进行相同的统计（SUM,AVG,COUNT…）。
MariaDB [haydnliao]> SELECT name, SUM(singin) AS singin, COUNT(*) AS count
    -> FROM login
    -> GROUP BY name
    -> WITH ROLLUP;
+------+--------+-------+
| name | singin | count |
+------+--------+-------+
| Li   |      2 |     1 |
| Ming |      7 |     3 |
| Wang |      7 |     2 |
| NULL |     16 |     6 |
+------+--------+-------+
4 rows in set (0.00 sec)
-- 其中记录 NULL 表示所有人的登录次数。
-- 使用 coalesce 来设置一个可以取代 NUll 的名称.
-- SELECT COALESCE(a,b,c);
/*
如果a==null,则选择b, 如果b==null,则选择c；
如果a!=null,则选择a；
如果a b c 都为null ，则返回为null（没意义）。
*/
MariaDB [haydnliao]> SELECT COALESCE(name, 'sum') AS name, SUM(singin) AS singin, COUNT(*) AS count
    -> FROM login
    -> GROUP BY name
    -> WITH ROLLUP;
+------+--------+-------+
| name | singin | count |
+------+--------+-------+
| Li   |      2 |     1 |
| Ming |      7 |     3 |
| Wang |      7 |     2 |
| sum  |     16 |     6 |
+------+--------+-------+
4 rows in set, 1 warning (0.00 sec)









