
-- 检测系统是否自带安装 mysql
[root@localhost ~]# rpm -qa | grep mysql

-- 普通删除模式和强力删除模式
[root@localhost ~]# rpm -e mysql　　
[root@localhost ~]# rpm -e --nodeps mysql　　

-- 安装 mysql
[root@localhost ~]# yum install mysql
[root@localhost ~]# yum install mysql-server
[root@localhost ~]# yum install mysql-deve

-- 启动 mysql
[root@localhost ~]# service mysqld start

-- 如果是 CentOS 7 版本, 可以使用 mariadb 代替
[root@localhost ~]# yum install mariadb-server mariadb 
[root@localhost ~]# systemctl start mariadb
[root@localhost ~]# systemctl stop mariadb
[root@localhost ~]# systemctl restart mariadb
-- 设置开机启动
[root@localhost ~]# systemctl enable mariadb

-- 使用 mysqladmin 工具来获取服务器状态
-- 版本文件在linux上位于 /usr/bin on linux ，在window上位于C:\mysql\bin。
[root@localhost ~]# mysqladmin --version

-- 在 MySQL Client 使用 mysql 命令连接到Mysql服务器上。
-- 默认情况下Mysql服务器的密码（root用户密码）为空。
-- 创建root用户密码，连接到服务器。
[root@localhost ~]# mysqladmin -u root password "new_password"
[root@localhost ~]# mysql -u root -p

-- 检查MySQL服务器是否启动。
[root@localhost ~]# ps -ef | grep mysqld

-- 启动mysql服务器。
[root@localhost ~]# cd /usr/bin
[root@localhost ~]# ./mysqld_safe &

-- 关闭目前运行的 MySQL 服务器。
[root@localhost ~]# cd /usr/bin
[root@localhost ~]# ./mysqladmin -u root -p shutdown

-- 添加用户 insert
MariaDB [(none)]> use mysql
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [mysql]> insert into user
    -> (host, user, password, select_priv, insert_priv, update_priv)
    -> values ('localhost', 'haydnliao', password('xxx'), 'y', 'y', 'y');

Query OK, 1 row affected, 4 warnings (0.00 sec)

MariaDB [mysql]> flush privileges;
Query OK, 0 rows affected (0.00 sec)

MariaDB [mysql]> SELECT host, user, password FROM user WHERE user='haydnliao';
+-----------+-----------+-------------------------------------------+
| host      | user      | password                                  |
+-----------+-----------+-------------------------------------------+
| localhost | haydnliao | *CFFC49ED451A7B8185E8F6D695F5274C65B0C10C |
+-----------+-----------+-------------------------------------------+
1 row in set (0.00 sec)

/*用户权限列表如下：
    Select_priv
    Insert_priv
    Update_priv
    Delete_priv
    Create_priv
    Drop_priv
    Reload_priv
    Shutdown_priv
    Process_priv
    File_priv
    Grant_priv
    References_priv
    Index_priv
    Alter_priv
*/

-- 添加用户 grant
MariaDB [(none)]> use mysql
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [mysql]> GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP
    -> ON TUTORIALS.*
    -> TO 'haydn'@'localhost'
    -> IDENTIFIED BY 'xxx';
Query OK, 0 rows affected (0.00 sec)

MariaDB [mysql]> SELECT host, user, password FROM user WHERE user='haydn';
+-----------+-------+-------------------------------------------+
| host      | user  | password                                  |
+-----------+-------+-------------------------------------------+
| localhost | haydn | *CFFC49ED451A7B8185E8F6D695F5274C65B0C10C |
+-----------+-------+-------------------------------------------+
1 row in set (0.00 sec)

-- 配置服务器使其允许远程连接 grant
MariaDB [(none)]> GRANT ALL PRIVILEGES
    -> ON *.*
    -> TO 'root'@'%'
    -> IDENTIFIED BY 'xxx'
    -> WITH GRANT OPTION;
Query OK, 0 rows affected (0.01 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.00 sec)

-- 管理MySQL的命令
-- USE 数据库名 :
-- 选择要操作的Mysql数据库，使用该命令后所有Mysql命令都只针对该数据库。
MariaDB [(none)]> use mysql
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
-- SHOW DATABASES: 
-- 列出 MySQL 数据库管理系统的数据库列表。
MariaDB [mysql]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| test               |
+--------------------+
4 rows in set (0.00 sec)
-- SHOW TABLES:
-- 显示指定数据库的所有表，使用该命令前需要使用 use 命令来选择要操作的数据库。
MariaDB [mysql]> SHOW TABLES;
+---------------------------+
| Tables_in_mysql           |
+---------------------------+
| columns_priv              |
| db                        |
| event                     |
| func                      |
| general_log               |
| help_category             |
| help_keyword              |
| help_relation             |
| help_topic                |
| host                      |
| ndb_binlog_index          |
| plugin                    |
| proc                      |
| procs_priv                |
| proxies_priv              |
| servers                   |
| slow_log                  |
| tables_priv               |
| time_zone                 |
| time_zone_leap_second     |
| time_zone_name            |
| time_zone_transition      |
| time_zone_transition_type |
| user                      |
+---------------------------+
24 rows in set (0.00 sec)
-- SHOW COLUMNS FROM table_name：
-- DESC table_name:
-- 显示数据表的属性，属性类型，主键信息 ，是否为 NULL，默认值等其他信息。
MariaDB [mysql]> SHOW COLUMNS FROM user;
+------------------------+-----------------------------------+------+-----+---------+-------+
| Field                  | Type                              | Null | Key | Default | Extra |
+------------------------+-----------------------------------+------+-----+---------+-------+
| Host                   | char(60)                          | NO   | PRI |         |       |
| User                   | char(16)                          | NO   | PRI |         |       |
| Password               | char(41)                          | NO   |     |         |       |
| Select_priv            | enum('N','Y')                     | NO   |     | N       |       |
| Insert_priv            | enum('N','Y')                     | NO   |     | N       |       |
| Update_priv            | enum('N','Y')                     | NO   |     | N       |       |
| Delete_priv            | enum('N','Y')                     | NO   |     | N       |       |
| Create_priv            | enum('N','Y')                     | NO   |     | N       |       |
| Drop_priv              | enum('N','Y')                     | NO   |     | N       |       |
| Reload_priv            | enum('N','Y')                     | NO   |     | N       |       |
| Shutdown_priv          | enum('N','Y')                     | NO   |     | N       |       |
| Process_priv           | enum('N','Y')                     | NO   |     | N       |       |
| File_priv              | enum('N','Y')                     | NO   |     | N       |       |
| Grant_priv             | enum('N','Y')                     | NO   |     | N       |       |
| References_priv        | enum('N','Y')                     | NO   |     | N       |       |
| Index_priv             | enum('N','Y')                     | NO   |     | N       |       |
| Alter_priv             | enum('N','Y')                     | NO   |     | N       |       |
| Show_db_priv           | enum('N','Y')                     | NO   |     | N       |       |
| Super_priv             | enum('N','Y')                     | NO   |     | N       |       |
| Create_tmp_table_priv  | enum('N','Y')                     | NO   |     | N       |       |
| Lock_tables_priv       | enum('N','Y')                     | NO   |     | N       |       |
| Execute_priv           | enum('N','Y')                     | NO   |     | N       |       |
| Repl_slave_priv        | enum('N','Y')                     | NO   |     | N       |       |
| Repl_client_priv       | enum('N','Y')                     | NO   |     | N       |       |
| Create_view_priv       | enum('N','Y')                     | NO   |     | N       |       |
| Show_view_priv         | enum('N','Y')                     | NO   |     | N       |       |
| Create_routine_priv    | enum('N','Y')                     | NO   |     | N       |       |
| Alter_routine_priv     | enum('N','Y')                     | NO   |     | N       |       |
| Create_user_priv       | enum('N','Y')                     | NO   |     | N       |       |
| Event_priv             | enum('N','Y')                     | NO   |     | N       |       |
| Trigger_priv           | enum('N','Y')                     | NO   |     | N       |       |
| Create_tablespace_priv | enum('N','Y')                     | NO   |     | N       |       |
| ssl_type               | enum('','ANY','X509','SPECIFIED') | NO   |     |         |       |
| ssl_cipher             | blob                              | NO   |     | NULL    |       |
| x509_issuer            | blob                              | NO   |     | NULL    |       |
| x509_subject           | blob                              | NO   |     | NULL    |       |
| max_questions          | int(11) unsigned                  | NO   |     | 0       |       |
| max_updates            | int(11) unsigned                  | NO   |     | 0       |       |
| max_connections        | int(11) unsigned                  | NO   |     | 0       |       |
| max_user_connections   | int(11)                           | NO   |     | 0       |       |
| plugin                 | char(64)                          | NO   |     |         |       |
| authentication_string  | text                              | NO   |     | NULL    |       |
+------------------------+-----------------------------------+------+-----+---------+-------+
42 rows in set (0.01 sec)
-- SHOW INDEX FROM table_name:
-- 显示数据表的详细索引信息，包括PRIMARY KEY（主键）。
MariaDB [mysql]> SHOW INDEX FROM user;
+-------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| Table | Non_unique | Key_name | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment |
+-------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
| user  |          0 | PRIMARY  |            1 | Host        | A         |        NULL |     NULL | NULL   |      | BTREE      |         |               |
| user  |          0 | PRIMARY  |            2 | User        | A         |           8 |     NULL | NULL   |      | BTREE      |         |               |
+-------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+
2 rows in set (0.00 sec)
-- SHOW TABLE STATUS [FROM db_name] [LIKE 'pattern'] \G: 
-- 该命令将输出Mysql数据库管理系统的性能及统计信息。
MariaDB [mysql]> SHOW TABLE STATUS;
+---------------------------+--------+---------+------------+------+----------------+-------------+--------------------+--------------+-----------+----------------+---------------------+---------------------+---------------------+-------------------+----------+----------------+---------------------------------------------------+
| Name                      | Engine | Version | Row_format | Rows | Avg_row_length | Data_length | Max_data_length    | Index_length | Data_free | Auto_increment | Create_time         | Update_time         | Check_time          | Collation         | Checksum | Create_options | Comment                                           |
+---------------------------+--------+---------+------------+------+----------------+-------------+--------------------+--------------+-----------+----------------+---------------------+---------------------+---------------------+-------------------+----------+----------------+---------------------------------------------------+
| columns_priv              | MyISAM |      10 | Fixed      |    0 |              0 |           0 | 227994731135631359 |         4096 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | utf8_bin          |     NULL |                | Column privileges                                 |
| db                        | MyISAM |      10 | Fixed      |    3 |            440 |        1320 | 123848989752688639 |         5120 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 23:05:53 | 2017-07-02 22:26:21 | utf8_bin          |     NULL |                | Database privileges                               |
| event                     | MyISAM |      10 | Dynamic    |    0 |              0 |           0 |    281474976710655 |         2048 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | utf8_general_ci   |     NULL |                | Events                                            |
| func                      | MyISAM |      10 | Fixed      |    0 |              0 |           0 | 162974011515469823 |         1024 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | utf8_bin          |     NULL |                | User defined functions                            |
| general_log               | CSV    |      10 | Dynamic    |    2 |              0 |           0 |                  0 |            0 |         0 |           NULL | NULL                | NULL                | NULL                | utf8_general_ci   |     NULL |                | General log                                       |
| help_category             | MyISAM |      10 | Dynamic    |   39 |             28 |        1092 |    281474976710655 |         3072 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | utf8_general_ci   |     NULL |                | help categories                                   |
| help_keyword              | MyISAM |      10 | Fixed      |  464 |            197 |       91408 |  55450570411999231 |        16384 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | utf8_general_ci   |     NULL |                | help keywords                                     |
| help_relation             | MyISAM |      10 | Fixed      | 1028 |              9 |        9252 |   2533274790395903 |        19456 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | utf8_general_ci   |     NULL |                | keyword-topic relation                            |
| help_topic                | MyISAM |      10 | Dynamic    |  508 |            886 |      450388 |    281474976710655 |        20480 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | utf8_general_ci   |     NULL |                | help topics                                       |
| host                      | MyISAM |      10 | Fixed      |    0 |              0 |           0 | 110056715893866495 |         2048 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | utf8_bin          |     NULL |                | Host privileges;  Merged with database privileges |
| ndb_binlog_index          | MyISAM |      10 | Dynamic    |    0 |              0 |           0 |    281474976710655 |         1024 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | latin1_swedish_ci |     NULL |                |                                                   |
| plugin                    | MyISAM |      10 | Dynamic    |    0 |              0 |           0 |    281474976710655 |         1024 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | utf8_general_ci   |     NULL |                | MySQL plugins                                     |
| proc                      | MyISAM |      10 | Dynamic    |    0 |              0 |         292 |    281474976710655 |         4096 |       292 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | utf8_general_ci   |     NULL |                | Stored Procedures                                 |
| procs_priv                | MyISAM |      10 | Fixed      |    0 |              0 |           0 | 239253730204057599 |         4096 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | utf8_bin          |     NULL |                | Procedure privileges                              |
| proxies_priv              | MyISAM |      10 | Fixed      |    2 |            693 |        1386 | 195062158860484607 |         5120 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | utf8_bin          |     NULL |                | User proxy privileges                             |
| servers                   | MyISAM |      10 | Fixed      |    0 |              0 |           0 | 433752939111120895 |         1024 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | utf8_general_ci   |     NULL |                | MySQL Foreign Servers table                       |
| slow_log                  | CSV    |      10 | Dynamic    |    2 |              0 |           0 |                  0 |            0 |         0 |           NULL | NULL                | NULL                | NULL                | utf8_general_ci   |     NULL |                | Slow log                                          |
| tables_priv               | MyISAM |      10 | Fixed      |    0 |              0 |           0 | 239535205180768255 |         4096 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | utf8_bin          |     NULL |                | Table privileges                                  |
| time_zone                 | MyISAM |      10 | Fixed      |    0 |              0 |           0 |   1970324836974591 |         1024 |         0 |              1 | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | utf8_general_ci   |     NULL |                | Time zones                                        |
| time_zone_leap_second     | MyISAM |      10 | Fixed      |    0 |              0 |           0 |   3659174697238527 |         1024 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | utf8_general_ci   |     NULL |                | Leap seconds information for time zones           |
| time_zone_name            | MyISAM |      10 | Fixed      |    0 |              0 |           0 |  55450570411999231 |         1024 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | utf8_general_ci   |     NULL |                | Time zone names                                   |
| time_zone_transition      | MyISAM |      10 | Fixed      |    0 |              0 |           0 |   4785074604081151 |         1024 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | utf8_general_ci   |     NULL |                | Time zone transitions                             |
| time_zone_transition_type | MyISAM |      10 | Fixed      |    0 |              0 |           0 |  10696049115004927 |         1024 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL                | utf8_general_ci   |     NULL |                | Time zone transition types                        |
| user                      | MyISAM |      10 | Dynamic    |    8 |             72 |         576 |    281474976710655 |         2048 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 23:05:53 | NULL                | utf8_bin          |     NULL |                | Users and global privileges                       |
+---------------------------+--------+---------+------------+------+----------------+-------------+--------------------+--------------+-----------+----------------+---------------------+---------------------+---------------------+-------------------+----------+----------------+---------------------------------------------------+
24 rows in set (0.01 sec)
-- 表名以help开头的表
MariaDB [mysql]> SHOW TABLE STATUS LIKE 'help%';
+---------------+--------+---------+------------+------+----------------+-------------+-------------------+--------------+-----------+----------------+---------------------+---------------------+------------+-----------------+----------+----------------+------------------------+
| Name          | Engine | Version | Row_format | Rows | Avg_row_length | Data_length | Max_data_length   | Index_length | Data_free | Auto_increment | Create_time         | Update_time         | Check_time | Collation       | Checksum | Create_options | Comment                |
+---------------+--------+---------+------------+------+----------------+-------------+-------------------+--------------+-----------+----------------+---------------------+---------------------+------------+-----------------+----------+----------------+------------------------+
| help_category | MyISAM |      10 | Dynamic    |   39 |             28 |        1092 |   281474976710655 |         3072 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL       | utf8_general_ci |     NULL |                | help categories        |
| help_keyword  | MyISAM |      10 | Fixed      |  464 |            197 |       91408 | 55450570411999231 |        16384 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL       | utf8_general_ci |     NULL |                | help keywords          |
| help_relation | MyISAM |      10 | Fixed      | 1028 |              9 |        9252 |  2533274790395903 |        19456 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL       | utf8_general_ci |     NULL |                | keyword-topic relation |
| help_topic    | MyISAM |      10 | Dynamic    |  508 |            886 |      450388 |   281474976710655 |        20480 |         0 |           NULL | 2017-07-02 22:26:21 | 2017-07-02 22:26:21 | NULL       | utf8_general_ci |     NULL |                | help topics            |
+---------------+--------+---------+------------+------+----------------+-------------+-------------------+--------------+-----------+----------------+---------------------+---------------------+------------+-----------------+----------+----------------+------------------------+
4 rows in set (0.00 sec)
-- 表名以time开头的表，\G表示查询结构按列打印
MariaDB [mysql]> SHOW TABLE STATUS LIKE 'time%' \G ;
*************************** 1. row ***************************
           Name: time_zone
         Engine: MyISAM
        Version: 10
     Row_format: Fixed
           Rows: 0
 Avg_row_length: 0
    Data_length: 0
Max_data_length: 1970324836974591
   Index_length: 1024
      Data_free: 0
 Auto_increment: 1
    Create_time: 2017-07-02 22:26:21
    Update_time: 2017-07-02 22:26:21
     Check_time: NULL
      Collation: utf8_general_ci
       Checksum: NULL
 Create_options: 
        Comment: Time zones
*************************** 2. row ***************************
           Name: time_zone_leap_second
         Engine: MyISAM
        Version: 10
     Row_format: Fixed
           Rows: 0
 Avg_row_length: 0
    Data_length: 0
Max_data_length: 3659174697238527
   Index_length: 1024
      Data_free: 0
 Auto_increment: NULL
    Create_time: 2017-07-02 22:26:21
    Update_time: 2017-07-02 22:26:21
     Check_time: NULL
      Collation: utf8_general_ci
       Checksum: NULL
 Create_options: 
        Comment: Leap seconds information for time zones
*************************** 3. row ***************************
           Name: time_zone_name
         Engine: MyISAM
        Version: 10
     Row_format: Fixed
           Rows: 0
 Avg_row_length: 0
    Data_length: 0
Max_data_length: 55450570411999231
   Index_length: 1024
      Data_free: 0
 Auto_increment: NULL
    Create_time: 2017-07-02 22:26:21
    Update_time: 2017-07-02 22:26:21
     Check_time: NULL
      Collation: utf8_general_ci
       Checksum: NULL
 Create_options: 
        Comment: Time zone names
*************************** 4. row ***************************
           Name: time_zone_transition
         Engine: MyISAM
        Version: 10
     Row_format: Fixed
           Rows: 0
 Avg_row_length: 0
    Data_length: 0
Max_data_length: 4785074604081151
   Index_length: 1024
      Data_free: 0
 Auto_increment: NULL
    Create_time: 2017-07-02 22:26:21
    Update_time: 2017-07-02 22:26:21
     Check_time: NULL
      Collation: utf8_general_ci
       Checksum: NULL
 Create_options: 
        Comment: Time zone transitions
*************************** 5. row ***************************
           Name: time_zone_transition_type
         Engine: MyISAM
        Version: 10
     Row_format: Fixed
           Rows: 0
 Avg_row_length: 0
    Data_length: 0
Max_data_length: 10696049115004927
   Index_length: 1024
      Data_free: 0
 Auto_increment: NULL
    Create_time: 2017-07-02 22:26:21
    Update_time: 2017-07-02 22:26:21
     Check_time: NULL
      Collation: utf8_general_ci
       Checksum: NULL
 Create_options: 
        Comment: Time zone transition types
5 rows in set (0.00 sec)

ERROR: No query specified

MariaDB [mysql]> SHOW TABLE STATUS LIKE 'time%' \G ;
*************************** 1. row ***************************
           Name: time_zone
         Engine: MyISAM
        Version: 10
     Row_format: Fixed
           Rows: 0
 Avg_row_length: 0
    Data_length: 0
Max_data_length: 1970324836974591
   Index_length: 1024
      Data_free: 0
 Auto_increment: 1
    Create_time: 2017-07-02 22:26:21
    Update_time: 2017-07-02 22:26:21
     Check_time: NULL
      Collation: utf8_general_ci
       Checksum: NULL
 Create_options: 
        Comment: Time zones
*************************** 2. row ***************************
           Name: time_zone_leap_second
         Engine: MyISAM
        Version: 10
     Row_format: Fixed
           Rows: 0
 Avg_row_length: 0
    Data_length: 0
Max_data_length: 3659174697238527
   Index_length: 1024
      Data_free: 0
 Auto_increment: NULL
    Create_time: 2017-07-02 22:26:21
    Update_time: 2017-07-02 22:26:21
     Check_time: NULL
      Collation: utf8_general_ci
       Checksum: NULL
 Create_options: 
        Comment: Leap seconds information for time zones
*************************** 3. row ***************************
           Name: time_zone_name
         Engine: MyISAM
        Version: 10
     Row_format: Fixed
           Rows: 0
 Avg_row_length: 0
    Data_length: 0
Max_data_length: 55450570411999231
   Index_length: 1024
      Data_free: 0
 Auto_increment: NULL
    Create_time: 2017-07-02 22:26:21
    Update_time: 2017-07-02 22:26:21
     Check_time: NULL
      Collation: utf8_general_ci
       Checksum: NULL
 Create_options: 
        Comment: Time zone names
*************************** 4. row ***************************
           Name: time_zone_transition
         Engine: MyISAM
        Version: 10
     Row_format: Fixed
           Rows: 0
 Avg_row_length: 0
    Data_length: 0
Max_data_length: 4785074604081151
   Index_length: 1024
      Data_free: 0
 Auto_increment: NULL
    Create_time: 2017-07-02 22:26:21
    Update_time: 2017-07-02 22:26:21
     Check_time: NULL
      Collation: utf8_general_ci
       Checksum: NULL
 Create_options: 
        Comment: Time zone transitions
*************************** 5. row ***************************
           Name: time_zone_transition_type
         Engine: MyISAM
        Version: 10
     Row_format: Fixed
           Rows: 0
 Avg_row_length: 0
    Data_length: 0
Max_data_length: 10696049115004927
   Index_length: 1024
      Data_free: 0
 Auto_increment: NULL
    Create_time: 2017-07-02 22:26:21
    Update_time: 2017-07-02 22:26:21
     Check_time: NULL
      Collation: utf8_general_ci
       Checksum: NULL
 Create_options: 
        Comment: Time zone transition types
5 rows in set (0.00 sec)

ERROR: No query specified
