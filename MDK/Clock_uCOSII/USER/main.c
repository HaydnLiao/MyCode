#include "task.h"

int main(void)
{
	Stm32_Clock_Init(9);	//系统时钟设置
	delay_init(72);			//延时初始化
	UART_Init(72, 115200);	//初始化USART1，波特率为115200

	OSInit();
	OSTaskCreate(StartTask, (void *)0, (OS_STK *)&START_TASK_STK[START_STK_SIZE-1], START_TASK_PRIO); //创建起始任务
	OSStart();
}
