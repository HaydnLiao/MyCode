#ifndef __TASK_H
#define __TASK_H

#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "includes.h"		//uCOSII

//START任务
#define START_TASK_PRIO	8
#define START_STK_SIZE	64
extern OS_STK START_TASK_STK[START_STK_SIZE];
void StartTask(void *pdata);

//LED任务
#include "led.h"
#define LED_TASK_PRIO	10
#define LED_STK_SIZE	64
extern OS_STK LED_TASK_STK[LED_STK_SIZE];
void LEDTask(void *pdata);

//RTC任务
#include "rtc.h"
#define RTC_TASK_PRIO	14
#define RTC_STK_SIZE	512
extern OS_STK RTC_TASK_STK[RTC_STK_SIZE];
void RTCTask(void *pdata);

//RTCReset任务
// #include "rtc.h"
#define RTCRESET_TASK_PRIO	12
#define RTCRESET_STK_SIZE	64
extern OS_STK RTCRESET_TASK_STK[RTCRESET_STK_SIZE];
void RTCResetTask(void *pdata);

//RTCAlarm任务
// #include "rtc.h"
#define RTCALARM_TASK_PRIO	28
#define RTCALARM_STK_SIZE	64
extern OS_STK RTCALARM_TASK_STK[RTCALARM_STK_SIZE];
void RTCAlarmTask(void *pdata);

//OLED任务
#include "oled_i2c.h"
#define OLED_TASK_PRIO	16
#define OLED_STK_SIZE	256
extern OS_STK OLED_TASK_STK[OLED_STK_SIZE];
void OLEDTask(void *pdata);

//DHT11任务
#include "dht11.h"
#define DHT11_TASK_PRIO	18
#define DHT11_STK_SIZE	64
extern OS_STK DHT11_TASK_STK[DHT11_STK_SIZE];
void DHT11Task(void *pdata);

//Key任务
#include "key.h"
#define KEY_TASK_PRIO	20
#define KEY_STK_SIZE	64
extern OS_STK KEY_TASK_STK[KEY_STK_SIZE];
void KeyTask(void *pdata);

//PIR任务
#include "pir.h"
#define PIR_TASK_PRIO	22
#define PIR_STK_SIZE	64
extern OS_STK PIR_TASK_STK[PIR_STK_SIZE];
void PIRTask(void *pdata);

//LightSensor任务
#include "ls.h"
#define LS_TASK_PRIO	24
#define LS_STK_SIZE		128
extern OS_STK LS_TASK_STK[LS_STK_SIZE];
void LSTask(void *pdata);

//TPAD任务
#include "tpad.h"
#define TPAD_TASK_PRIO	26
#define TPAD_STK_SIZE	128
extern OS_STK TPAD_TASK_STK[TPAD_STK_SIZE];
void TPADTask(void *pdata);

//Beep任务
#include "beep.h"
#define BEEP_TASK_PRIO	30
#define BEEP_STK_SIZE	64
extern OS_STK BEEP_TASK_STK[BEEP_STK_SIZE];
void BeepTask(void *pdata);


//将alarmValue转为字符串存储在alarmValueStr中
u8* AlarmValueToString(void);

#endif
