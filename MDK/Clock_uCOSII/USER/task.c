#include "task.h"

//任务堆栈定义
OS_STK START_TASK_STK[START_STK_SIZE];
OS_STK LED_TASK_STK[LED_STK_SIZE];
OS_STK RTC_TASK_STK[RTC_STK_SIZE];
OS_STK RTCRESET_TASK_STK[RTCRESET_STK_SIZE];
OS_STK RTCALARM_TASK_STK[RTCALARM_STK_SIZE];
OS_STK OLED_TASK_STK[OLED_STK_SIZE];
OS_STK DHT11_TASK_STK[DHT11_STK_SIZE];
OS_STK KEY_TASK_STK[KEY_STK_SIZE];
OS_STK PIR_TASK_STK[PIR_STK_SIZE];
OS_STK LS_TASK_STK[LS_STK_SIZE];
OS_STK TPAD_TASK_STK[TPAD_STK_SIZE];
OS_STK BEEP_TASK_STK[BEEP_STK_SIZE];

u32 alarmValue = 0;
u8 alarmValueStr[3];

void StartTask(void *pdata){
	OS_CPU_SR cpu_sr=0;
	pdata = pdata;
	OS_ENTER_CRITICAL();			//进入临界段（无法中断）   
	OSTaskCreate(LEDTask, (void *)0, (OS_STK *)&LED_TASK_STK[LED_STK_SIZE-1], LED_TASK_PRIO);
	OSTaskCreate(RTCTask, (void *)0, (OS_STK *)&RTC_TASK_STK[RTC_STK_SIZE-1], RTC_TASK_PRIO);
	OSTaskCreate(RTCResetTask, (void *)0, (OS_STK *)&RTCRESET_TASK_STK[RTCRESET_STK_SIZE-1], RTCRESET_TASK_PRIO);
	OSTaskCreate(RTCAlarmTask, (void *)0, (OS_STK *)&RTCALARM_TASK_STK[RTCALARM_STK_SIZE-1], RTCALARM_TASK_PRIO);
	OSTaskCreate(OLEDTask, (void *)0, (OS_STK *)&OLED_TASK_STK[OLED_STK_SIZE-1], OLED_TASK_PRIO);
	OSTaskCreate(DHT11Task, (void *)0, (OS_STK *)&DHT11_TASK_STK[DHT11_STK_SIZE-1], DHT11_TASK_PRIO);
	OSTaskCreate(KeyTask, (void *)0, (OS_STK *)&KEY_TASK_STK[KEY_STK_SIZE-1], KEY_TASK_PRIO);
	OSTaskCreate(PIRTask, (void *)0, (OS_STK *)&PIR_TASK_STK[PIR_STK_SIZE-1], PIR_TASK_PRIO);
	OSTaskCreate(LSTask, (void *)0, (OS_STK *)&LS_TASK_STK[LS_STK_SIZE-1], LS_TASK_PRIO);
	OSTaskCreate(TPADTask, (void *)0, (OS_STK *)&TPAD_TASK_STK[TPAD_STK_SIZE-1], TPAD_TASK_PRIO);
	OSTaskCreate(BeepTask, (void *)0, (OS_STK *)&BEEP_TASK_STK[BEEP_STK_SIZE-1], BEEP_TASK_PRIO);
	OSTaskSuspend(START_TASK_PRIO);	//挂起起始任务
	OS_EXIT_CRITICAL();				//退出临界段（可以中断）
}

//LED任务
void LEDTask(void *pdata){
	LED_Init();
	pdata = pdata;

	while(1){
		LED0_ON;
		delay_ms(100);
		LED0_OFF;
		delay_ms(1000);
	}
}

//RTC任务
void RTCTask(void *pdata){
	u8 buf[128];
	u16 len;
	calendar tempCal;
	while(RTC_Init()){
		// printf("RTC_Init()\r\n");
		delay_ms(500);
	}
	pdata = pdata;

	while(1){
		// RTC_Print_Calendar(&cal);
		USART1_Receive_Data(buf, &len);
		if(len != 0){
			// 从USART1得到要设置的时间
			String_To_Calendar(buf, len, &tempCal);
			RTC_Set_Calendar(tempCal);	//设置时间
		}
		delay_ms(300);
	}
}

//RTCReset任务
void RTCResetTask(void *pdata){
	pdata = pdata;

	while(1){
		if(RTC_RESET_FLAG){
			RTC_Reset();
			//系统重启，不需要将RTC_RESET_FLAG置为0
		}
		delay_ms(300);
	}
}

//RTCAlarm任务
void RTCAlarmTask(void *pdata){
	pdata = pdata;

	while(1){
		if(RTC_ALARM_STATUS){
			BEEP_STATUS = 1;
		}
		delay_ms(300);
	}
}

//OLED任务
void OLEDTask(void *pdata){
	u8 date[20];
	u8 time[10];
	u16 ld = 20;
	u16 lt = 10;
	u8 th[15];
	u16 l = 15;
	u8 temp[3];
	u16 len = 3;

	u8 turn_flag = 0;
	u8 yesCount = 0;
	u8 maxCount = 30;

	I2C_Configuration();	//初始化I2C2接口
	OLED_Init();			//OLED初始化
	pdata = pdata;

	OLED_Open_Flash();
	OLED_ShowStr(12, 0, (unsigned char*)"Haydn's Clock", 2);	//标题栏

	while(1){
		Calendar_To_Date_Time(&cal, date, ld, time, lt);
		if(OLED_ALARM_SET == 0){
			if(OLED_DISPLAY_MODE){	//交替闪烁显示模式
				if(yesCount == 0){
					OLED_Clear_Line(2, 7);
				}
				if(!turn_flag){
					OLED_ShowStr(0, 3, date, 2);	//日期，8*16字符
					OLED_ShowStr(32, 5, time, 2);	//时间，8*16字符
				}else{
					yesCount += 1;	//温湿度显示的时长减短为原来的二分之一

					OLED_ShowCN(20, 3, 0);
					OLED_ShowCN(36, 3, 1);
					OLED_ShowCN(52, 3, 2);
					Temperature_To_String(temp, len);
					OLED_ShowStr(68, 3, temp, 2);
					OLED_ShowCN(84, 3, 3);

					OLED_ShowCN(20, 5, 4);
					OLED_ShowCN(36, 5, 5);
					OLED_ShowCN(52, 5, 2);
					Humidity_To_String(temp, len);
					OLED_ShowStr(68, 5, temp, 2);
					OLED_ShowCN(84, 5, 6);
				}
				if(++yesCount > maxCount){
					turn_flag = !turn_flag;
					yesCount = 0;
					OLED_Clear_Line(2, 7);
				}
			}else{	//默认模式
				if(yesCount > 0){
					yesCount = 0;
					OLED_Clear_Line(2, 7);
				}
				OLED_ShowStr(0, 3, date, 1);	//日期，6*8字符
				OLED_ShowStr(32, 4, time, 2);	//时间，8*16字符
				DHT11_To_String(th, l);
				OLED_ShowStr(56, 6, th, 1);		//温湿度，6*8字符
			}
		}
		delay_ms(100);
	}
}

//DHT11任务
void DHT11Task(void *pdata){
	while(DHT11_Init()){
		// printf("DHT11_Init()\r\n");
		delay_ms(500);
	}
	pdata = pdata;

	while(1){
		// OSSchedLock();
		while(DHT11_Read_Data(&temperature, &humidity));
		// OSSchedUnlock();
		// printf("temperature: %d℃  humidity: %d%%\r\n", temperature, humidity);
		delay_ms(1000);
	}
}

//Key任务
void KeyTask(void *pdata){
	Key_Init();
	pdata = pdata;

	while(1){
		if(KEY1_DOWN || KEY2_DOWN){
			delay_ms(10);	//消抖
			if(KEY1_DOWN){
				LED2_ON;
				if(OLED_ALARM_SET == 0){
					if(OLED_STATUS == 0){
						OLED_STATUS = 1;
						OLED_ON();
					}
					OLED_DISPLAY_MODE_CHANGE();
				}else{
					alarmValue -= 1;
					if(alarmValue > 24){	//unsigned没有负值
						alarmValue = 24;
					}
					OLED_ShowStr(32, 6, AlarmValueToString(), 2);
				}
				while(KEY1_DOWN);	//等待松开按键
				LED2_OFF;
			}else{
				LED2_ON;
				if(OLED_ALARM_SET == 0){
					if(OLED_STATUS == 0){	//OLED状态(0: 关闭; 1: 开启)
						OLED_ON();
					}else{
						OLED_OFF();
					}
					OLED_STATUS = !OLED_STATUS;
				}else{
					alarmValue += 1;
					if(alarmValue > 24){
						alarmValue = 0;
					}
					OLED_ShowStr(32, 6, AlarmValueToString(), 2);
				}
				while(KEY2_DOWN);	//等待松开按键
				LED2_OFF;
			}
		}
		delay_ms(100);
	}
}

//PIR任务
void PIRTask(void *pdata){
	u8 yesCount = 0, noCount = 0;
	u8 pir_open_oled_flag = 0;

	PIR_Init();
	pdata = pdata;

	while(1){
		if(OLED_STATUS == 0 || pir_open_oled_flag){	//OLED关闭或者被PIR开启
			if(PIR_CHECKITEMS){
				yesCount += 1;
				noCount = 0;
			}else{
				noCount += 1;
				yesCount = 0;
			}
			if(yesCount > 3){
				yesCount = 0;
				if(OLED_STATUS == 0){	//OLED关闭则开启OLED屏
					pir_open_oled_flag = 1;
					OLED_STATUS = 1;
					OLED_ON();
				}
			}else if(noCount > 3){		//OLED开启则关闭OLED屏
				noCount = 0;
				if(OLED_STATUS){
					pir_open_oled_flag = 0;
					OLED_STATUS = 0;
					OLED_OFF();
				}
			}
		}else{
			yesCount = 0;
			noCount = 0;
		}
		// printf("PIR_CHECKITEMS: %d\r\n", PIR_CHECKITEMS);
		delay_ms(1000);
	}
}

//LightSensor任务
void LSTask(void *pdata){
	u16 lightIntensity = 0;
	u16 oldLightIntensity = 0;

	LS_Init();
	pdata = pdata;

	while(1){
		lightIntensity = Get_LS_Value();
		if(lightIntensity - oldLightIntensity > 200 || oldLightIntensity - lightIntensity > 500){	//光照变化大时才调整
			OLED_Set_Brightness((4096 - lightIntensity) * 0xff / 4096 & 0xff);
			oldLightIntensity = lightIntensity;
		}
		// printf("lightIntensity: %d\r\n", lightIntensity);
		delay_ms(1000);
	}
}

//TPAD任务
void TPADTask(void *pdata){
	while(TPAD_Init(72)){
		// printf("TPAD_Init()\r\n");
		delay_ms(500);
	}
	pdata = pdata;
	while(1){
		if(TPAD_Scan(0)){
			LED2_ON;
			if(OLED_ALARM_SET == 0){
				if(RTC_ALARM_STATUS == 0){
					OLED_ALARM_SET = 1;

					if(OLED_STATUS == 0){	//OLED状态(0: 关闭; 1: 开启)
						OLED_STATUS = 1;
						OLED_ON();
					}

					OLED_Clear_Line(2, 7);
					OLED_ShowCN(48, 3, 15);	//闹
					OLED_ShowCN(64, 3, 8);	//钟
					OLED_ShowStr(32, 6, AlarmValueToString(), 2);
					OLED_ShowCN(48, 6, 16);	//小
					OLED_ShowCN(64, 6, 7);	//时
					OLED_ShowCN(80, 6, 17);	//后
				}else{
					RTC_ALARM_STATUS = 0;
					BEEP_STATUS = 0;
				}
			}else{
				RTC_ALARM_STATUS = 0;
				RTC_Set_Alarm(alarmValue);	//为0时关闭闹钟，单位为小时（测试时单位为秒）
				OLED_Clear_Line(6, 7);
				OLED_ShowCN(32, 6, 18);		//设
				OLED_ShowCN(48, 6, 19);		//置
				OLED_ShowCN(64, 6, 20);		//完
				OLED_ShowCN(80, 6, 21);		//成
				delay_ms(500);
				OLED_Clear_Line(2, 7);
				OLED_ALARM_SET = 0;
			}
			delay_ms(500);
			LED2_OFF;
		}
		delay_ms(300);
	}
}

//Beep任务
void BeepTask(void *pdata){
	Beep_Init();
	pdata = pdata;

	while(1){
		if(BEEP_STATUS){
			BEEP_ON;
			delay_ms(100);
			BEEP_OFF;
			delay_ms(50);
			BEEP_ON;
			delay_ms(100);
		}
		BEEP_OFF;
		delay_ms(1000);
	}
}


//将alarmValue转为字符串存储在alarmValueStr中
u8* AlarmValueToString(){
	alarmValueStr[0] = alarmValue/10==0 ? ' ' : alarmValue/10+'0';
	alarmValueStr[1] = alarmValue % 10 + '0';
	alarmValueStr[2] = '\0';
	return alarmValueStr;
}
