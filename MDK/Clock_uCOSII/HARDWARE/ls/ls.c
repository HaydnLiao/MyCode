#include "ls.h"
#include "adc.h"

//LightSensor初始化
void LS_Init(void){
	ADC_Init();
}

//获得光敏传感器的模拟量
u16 Get_LS_Value(void){
	return	Get_ADC_Average(6, 20);	//ADC1 通道6 PA6 20次取平均值
}

//获得光敏传感器的电压
float Get_LS_Voltage(void){
	return	Get_ADC_Average(6, 20) * 3.3 / 4096;	//ADC1 通道6 PA6 20次取平均值 转换成电压
}

