#ifndef __LS_H
#define __LS_H

#include "sys.h"

void LS_Init(void);
u16 Get_LS_Value(void);
float Get_LS_Voltage(void);

#endif
