#ifndef __RTC_H
#define __RTC_H

#include "sys.h"

//时间结构体
typedef struct 
{
	vu8 hour;
	vu8 minute;
	vu8 second;
	vu16 year;
	vu8 month;
	vu8 day;
	vu8 week;
}calendar;
extern calendar cal;
extern u8 RTC_ALARM_STATUS;

u8 RTC_Init(void);
void RTC_IRQHandler(void);
u8 RTC_Set(u16 year, u8 month, u8 day, u8 hour, u8 minute, u8 second);
u8 Is_Leap_Year(u16 year);
u8 RTC_Get(void);
u8 RTC_Get_Week(u16 year, u8 month, u8 day);
u8 RTC_Set_Calendar(calendar c);
void RTC_Reset(void);

u8 String_To_Calendar(u8 *s, u16 l, calendar *c);
u8 Calendar_To_String(calendar *c, u8 *s, u16 l);
u8 Calendar_To_Date_Time(calendar *c, u8 *d, u16 ld, u8 *t, u16 lt);
void RTC_Print_Calendar(calendar *c);
void RTC_Set_Alarm(u32 hours);

#endif
