#include "oled_i2c.h"
#include "delay.h"

//OLED显示模式(0: 默认; 1: 交替闪烁显示)
unsigned char OLED_DISPLAY_MODE = 0;
//OLED状态(0: 关闭; 1: 开启)
unsigned char OLED_STATUS = 1;
//是否进入闹钟设置
unsigned char OLED_ALARM_SET = 0;
unsigned char oled_online_flag;

//初始化硬件I2C引脚
void I2C_Configuration(void)
{
	RCC->APB1ENR|=1<<22;	//I2C2时钟使能
	RCC->APB2ENR|=1<<3;		//PortB时钟使能

	/*STM32F103VET6芯片的硬件I2C: PB10 -- SCL; PB11 -- SDA */
	GPIOB->CRH&=0xFFFF00FF;
	GPIOB->CRH|=0x0000FF00;	//PB10、PB11复用开漏输出

	//使用I2C2
	RCC->APB1RSTR|=1<<22;		//复位I2C2
	RCC->APB1RSTR&=~(1<<22);	//复位完成

	I2C2->CR1=0x0401;	//I2C模块使能和应答使能
	I2C2->CR2=0x0024;	//I2C模块时钟频率FREQ=36MHz
	I2C2->CCR=0x801E;	//设置主模式为快速模式和设置SCL时钟
	I2C2->OAR1=0x4030;	//主机的I2C地址,随意设置，这里为0x18
	I2C2->TRISE=0x000B;	//设置最大上升空间

}

//向OLED寄存器地址写一个byte的数据
int I2C_WriteByte(uint8_t addr, uint8_t data)
{
	uint8_t address=OLED_ADDRESS;
	unsigned char time;
	oled_online_flag = 1;
	time = 0;
	while(I2C2->SR2&0x0002) {	//在总线上正在进行数据通讯
		if (time++ > CHECK_TIME) {
			oled_online_flag = 0;
			break;
		}
	}
	I2C2->CR1|=1<<8;	//开启I2C2
	time = 0;
	while(!((u16)(I2C2->SR1)&(u16)(0x0001)) && oled_online_flag == 1) {	//起始条件未发送
		if (time++ > CHECK_TIME) {
			oled_online_flag = 0;
			break;
		}
	}
	time = 0;
	while(!((u16)(I2C2->SR2)&(u16)(0x0003))==0x0003 && oled_online_flag == 1) {	//主模式
		if (time++ > CHECK_TIME) {
			oled_online_flag = 0;
			break;
		}
	}
	address &= 0xFFFE;	//器件地址，默认0x78
	I2C2->DR=address;
	time = 0;
	while(!(((u16)(I2C2->SR1)&(u16)(0x0082))==0x0082) && oled_online_flag == 1) {	//数据未发送完成
		if (time++ > CHECK_TIME) {
			oled_online_flag = 0;
			break;
		}
	}
	time = 0;
	while(!(((u16)(I2C2->SR2)&(u16)(0x0007))==0x0007) && oled_online_flag == 1) {
		if (time++ > CHECK_TIME) {
			oled_online_flag = 0;
			break;
		}
	}

	I2C2->DR=addr;	//器件寄存器地址
	time = 0;
	while (!(((u16)(I2C2->SR1)&(u16)(0x0080))==0x0080) && oled_online_flag == 1) {	//数据寄存器非空
		if (time++ > CHECK_TIME) {
			oled_online_flag = 0;
			break;
		}
	}
	time = 0;
	while(!(((u16)(I2C2->SR2)&(u16)(0x0007))==0x0007) && oled_online_flag == 1){
		if (time++ > CHECK_TIME) {
			oled_online_flag = 0;
			break;
		}
	}

	I2C2->DR=data;	//发送数据
	time = 0;
	while (!(((u16)(I2C2->SR1)&(u16)(0x0080))==0x0080) && oled_online_flag == 1) {
		if (time++ > CHECK_TIME) {
			oled_online_flag = 0;
			break;
		}
	}
	time = 0;
	while(!(((u16)(I2C2->SR2)&(u16)(0x0007))==0x0007) && oled_online_flag == 1) {
		if (time++ > CHECK_TIME) {
			oled_online_flag = 0;
			break;
		}
	}
	
	I2C2->CR1|=1<<9;	//产生停止条件，关闭I2C2总线
	if (!oled_online_flag) {
		return 0;
	} else {
		return 1;
	}
}

//向OLED写入命令
int OLED_WriteCmd(unsigned char I2C_Command)
{
	return I2C_WriteByte(0x00, I2C_Command);
}

//向OLED写入数据
int OLED_WriteData(unsigned char I2C_Data)
{
	return I2C_WriteByte(0x40, I2C_Data);
}

//初始化OLED
int OLED_Init(void)
{
	if (!oled_online_flag) {
		delay_ms(100); //这里的延时很重要

		OLED_WriteCmd(0xAE);	//关闭显示
		OLED_WriteCmd(0x20);	//设置内存地址模式
		OLED_WriteCmd(0x10);	//00,Horizontal Addressing Mode;01,Vertical Addressing Mode;10,Page Addressing Mode (RESET);11,Invalid
		OLED_WriteCmd(0xb0);	//Set Page Start Address for Page Addressing Mode,0-7
		OLED_WriteCmd(0xc8);	//Set COM Output Scan Direction
		OLED_WriteCmd(0x00);	//---set low column address
		OLED_WriteCmd(0x10);	//---set high column address
		OLED_WriteCmd(0x40);	//--set start line address
		OLED_WriteCmd(0x81);	//--set contrast control register
		OLED_WriteCmd(0xff);	//亮度调节 0x00~0xff
		OLED_WriteCmd(0xa1);	//--set segment re-map 0 to 127
		OLED_WriteCmd(0xa6);	//--set normal display
		OLED_WriteCmd(0xa8);	//--set multiplex ratio(1 to 64)
		OLED_WriteCmd(0x3F);
		OLED_WriteCmd(0xa4);	//0xa4,Output follows RAM content;0xa5,Output ignores RAM content
		OLED_WriteCmd(0xd3);	//-set display offset
		OLED_WriteCmd(0x00);	//-not offset
		OLED_WriteCmd(0xd5);	//--set display clock divide ratio/oscillator frequency
		OLED_WriteCmd(0xf0);	//--set divide ratio
		OLED_WriteCmd(0xd9);	//--set pre-charge period
		OLED_WriteCmd(0x22);
		OLED_WriteCmd(0xda);	//--set com pins hardware configuration
		OLED_WriteCmd(0x12);
		OLED_WriteCmd(0xdb);	//--set vcomh
		OLED_WriteCmd(0x20);	//0x20,0.77xVcc
		OLED_WriteCmd(0x8d);	//--set DC-DC enable
		OLED_WriteCmd(0x14);
		OLED_WriteCmd(0xaf);	//--turn on oled panel

		delay_ms(100);		//延时以加大两次采集时间间隔，防止采集过快时传感器响应不过来，太长容易导致超时
		OLED_Fill(0xFF);	//全屏点亮
		delay_ms(100);
		OLED_Fill(0x00);	//全屏灭
	}

	if (!oled_online_flag) {
		return 0;
	} else {
		return 1;
	}
}

//OLED全屏填充
void OLED_Fill(unsigned char fill_Data)
{
	unsigned char m,n;
	for(m=0; m<8; m++){
		OLED_WriteCmd(0xb0+m);	//page0-page1
		OLED_WriteCmd(0x00);	//low column start address
		OLED_WriteCmd(0x10);	//high column start address
		for(n=0; n<128; n++){
			OLED_WriteData(fill_Data);
		}
	}
}

//OLED清屏
void OLED_CLS(void)
{
	OLED_Fill(0x00);
}

//将OLED从休眠中唤醒
void OLED_ON(void)
{
	OLED_WriteCmd(0x8D);	//设置电荷泵
	OLED_WriteCmd(0x14);	//开启电荷泵
	OLED_WriteCmd(0xAF);	//OLED唤醒
}

//让OLED休眠，休眠模式下,OLED功耗不到10uA
void OLED_OFF(void)
{
	OLED_WriteCmd(0x8D);	//设置电荷泵
	OLED_WriteCmd(0x10);	//关闭电荷泵
	OLED_WriteCmd(0xAE);	//OLED休眠
}

//设置OLED光标（起始点坐标）
void OLED_SetPos(unsigned char x, unsigned char y)
{ 
	OLED_WriteCmd(0xb0+y);
	OLED_WriteCmd(((x&0xf0)>>4)|0x10);
	OLED_WriteCmd((x&0x0f)|0x01);
}

//显示中的ASCII字符,有6*8和8*16可选择
//字符大小(1: 6*8; 2: 8*16)
void OLED_ShowStr(unsigned char x, unsigned char y, unsigned char ch[], unsigned char textSize)
{
	unsigned char c = 0, i = 0, j = 0;
	switch(textSize){
		case 1:
			while(ch[j] != '\0'){
				c = ch[j] - 32;
				if(x > 126){
					x = 0;
					y++;
				}
				OLED_SetPos(x,y);
				for(i=0; i<6; i++){
					OLED_WriteData(f6x8[c][i]);
				}
				x += 6;
				j++;
			}
			break;
		case 2:
			while(ch[j] != '\0'){
				c = ch[j] - 32;
				if(x > 120){
					x = 0;
					y += 2;
				}
				OLED_SetPos(x,y);
				for(i=0; i<8; i++){
					OLED_WriteData(f8x16[c*16+i]);
				}
				OLED_SetPos(x,y+1);
				for(i=0; i<8; i++){
					OLED_WriteData(f8x16[c*16+i+8]);
				}
				x += 8;
				j++;
			}
			break;
	}
}

//显示16*16点阵的汉字
void OLED_ShowCN(unsigned char x, unsigned char y, unsigned char n)
{
	unsigned char wm=0;
	unsigned int adder=32*n;	//第n个汉字
	OLED_SetPos(x , y);
	for(wm = 0; wm < 16; wm++){
		OLED_WriteData(f16x16[adder]);
		adder += 1;
	}
	OLED_SetPos(x,y + 1);
	for(wm = 0;wm < 16;wm++){
		OLED_WriteData(f16x16[adder]);
		adder += 1;
	}
}

//显示BMP位图
void OLED_DrawBMP(unsigned char x0,unsigned char y0,unsigned char x1,unsigned char y1,unsigned char bmp[])
{
	unsigned int j=0;
	unsigned char x,y;
	for(y=y0; y<y1; y++){
		OLED_SetPos(x0,y);
		for(x=x0; x<x1; x++){
			OLED_WriteData(bmp[j++]);
		}
	}
}

//OLED清除行(0~7)
void OLED_Clear_Line(unsigned char a, unsigned char b)
{
	unsigned char i, j;
	for(i=a; i<=b; i++){
		OLED_SetPos(0, i);
		for(j=0; j<128; j++){
			OLED_WriteData(0x00);
		}
	}
}

//OLED开机动画
void OLED_Open_Flash(void)
{
	//标题栏
	OLED_ShowCN(8, 0, 7);		//时
	OLED_ShowCN(24, 0, 8);		//钟
	OLED_ShowStr(48, 0, (unsigned char *)"By:", 2);	//8*16字符
	OLED_ShowCN(72, 0, 9);		//廖
	OLED_ShowCN(88, 0, 10);		//昌
	OLED_ShowCN(104, 0, 11);	//海

	OLED_DrawBMP(0,2,128,8, (unsigned char *)bmp1);
	delay_ms(50);
	OLED_DrawBMP(0,2,128,8, (unsigned char *)bmp2);
	delay_ms(50);
	OLED_DrawBMP(0,2,128,8, (unsigned char *)bmp3);
	delay_ms(50);
	OLED_DrawBMP(0,2,128,8, (unsigned char *)bmp4);
	delay_ms(50);
	OLED_DrawBMP(0,2,128,8, (unsigned char *)bmp5);
	delay_ms(1000);
	OLED_CLS();
}

//OLED亮度调节
void OLED_Set_Brightness(unsigned char b)
{
	OLED_WriteCmd(0x81);
	OLED_WriteCmd(b);	//亮度调节 0x00~0xff
}

