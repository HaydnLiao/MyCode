#ifndef __OLEDFONT_H
#define __OLEDFONT_H

extern const unsigned char f16x16[];
extern const unsigned char f6x8[][6];
extern const unsigned char f8x16[];
extern const unsigned char bmp1[];
extern const unsigned char bmp2[];
extern const unsigned char bmp3[];
extern const unsigned char bmp4[];
extern const unsigned char bmp5[];

#endif
