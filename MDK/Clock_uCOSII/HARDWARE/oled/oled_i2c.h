#ifndef __OLED_I2C_H
#define __OLED_I2C_H

#include "sys.h"
#include "oledfont.h"

#define OLED_ADDRESS	0x78 //通过调整0R电阻，有0x78和0x7A两个地址，默认0x78
#define CHECK_TIME		250

#define OLED_DISPLAY_MODE_CHANGE()	(OLED_DISPLAY_MODE = OLED_DISPLAY_MODE ? 0 : 1)

//OLED显示模式(0: 默认; 1: 交替闪烁显示)
extern unsigned char OLED_DISPLAY_MODE;
//OLED状态(0: 开启; 1: 关闭)
extern unsigned char OLED_STATUS;
extern unsigned char OLED_ALARM_SET;

void I2C_Configuration(void);
int I2C_WriteByte(uint8_t addr, uint8_t data);
int OLED_WriteCmd(unsigned char I2C_Command);
int OLED_WriteData(unsigned char I2C_Data);
int OLED_Init(void);
void OLED_Fill(unsigned char fill_Data);
void OLED_CLS(void);
void OLED_ON(void);
void OLED_OFF(void);
void OLED_SetPos(unsigned char x, unsigned char y);
void OLED_ShowStr(unsigned char x, unsigned char y, unsigned char ch[], unsigned char textSize);
void OLED_ShowCN(unsigned char x, unsigned char y, unsigned char n);
void OLED_DrawBMP(unsigned char x0,unsigned char y0,unsigned char x1,unsigned char y1,unsigned char bmp[]);
void OLED_Clear_Line(unsigned char a, unsigned char b);
void OLED_Open_Flash(void);
void OLED_Set_Brightness(unsigned char b);

#endif
