#ifndef __TPAD_H
#define __TPAD_H

#include "sys.h"

#define TPAD_ARR_MAX_VAL	0x0ffff	//最大的自动重装载的值
#define TPAD_GATE_VAL		80		//触摸的门限值，也就是必须大于tpad_default_val+TPAD_GATE_VAL才认为是有效触摸

void TIM5_CH2_Init(u16 arr, u16 psc);
u8 TPAD_Init(u8 systick);
void TPAD_Reset(void);
u16 TPAD_Get_Val(void);
u16 TPAD_Get_MaxVal(u8 n);
u8 TPAD_Scan(u8 mode);

#endif
