#ifndef __BEEP_H
#define __BEEP_H

#include "sys.h"

#define BEEP PAout(8)
#define BEEP_ON (BEEP = 1)
#define BEEP_OFF (BEEP = 0)

extern u8 BEEP_STATUS;

void Beep_Init(void);

#endif
