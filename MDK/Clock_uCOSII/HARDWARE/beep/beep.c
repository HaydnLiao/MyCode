#include "beep.h"

u8 BEEP_STATUS = 0;

void Beep_Init(void){
	RCC->APB2ENR|=1<<2;		//使能PortA时钟
	GPIOA->CRH&=0xfffffff0;	//PA8
	GPIOA->CRH|=0x00000003;	//通用推挽输出
	GPIOA->ODR&=~(1<<8);	//关闭蜂鸣器
}
