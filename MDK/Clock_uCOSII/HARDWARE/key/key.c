#include "key.h"

void Key_Init(void){
	RCC->APB2ENR|=1<<2;	//使能PortA时钟
	RCC->APB2ENR|=1<<4;	//使能PortC时钟
	GPIOA->CRL&=0xfffffff0;
	GPIOA->CRL|=0x00000008;	//PA0 输入 内部下拉
	GPIOC->CRH&=0xff0fffff;
	GPIOC->CRH|=0x00800000;	//PC13 输入 内部下拉
}
