#ifndef __KEY_H
#define __KEY_H

#include "sys.h"

#define KEY1 PAin(0)	//K1
#define KEY2 PCin(13)	//K2

#define KEY1_DOWN	(KEY1 == 1)	//按下K1
#define KEY2_DOWN	(KEY2 == 1)	//按下K2

void Key_Init(void);

#endif
