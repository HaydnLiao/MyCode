#include "led.h"

void LED_Init(void){
	RCC->APB2ENR|=1<<3;			//使能PortB时钟

	GPIOB->CRL&=0xfffffff0;	//PB0
	GPIOB->CRL|=0x00000003;	//通用推挽输出
	GPIOB->ODR|=1<<0;

	GPIOB->CRL&=0xffffff0f;	//PB1
	GPIOB->CRL|=0x00000030;	//通用推挽输出
	GPIOB->ODR|=1<<1;

	GPIOB->CRL&=0xff0fffff;	//PB5
	GPIOB->CRL|=0x00300000;	//通用推挽输出
	GPIOB->ODR|=1<<5;
}
