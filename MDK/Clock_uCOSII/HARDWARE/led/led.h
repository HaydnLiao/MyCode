#ifndef __LED_H
#define __LED_H

#include "sys.h"

#define LED0 PBout(0)	//G
#define LED1 PBout(1)	//B
#define LED2 PBout(5)	//R

#define LED0_ON	(LED0 = 0)
#define LED0_OFF	(LED0 = 1)
#define LED0_TOGGLE (LED0=!LED0)
#define LED1_ON	(LED1 = 0)
#define LED1_OFF	(LED1 = 1)
#define LED1_TOGGLE (LED1=!LED1)
#define LED2_ON	(LED2 = 0)
#define LED2_OFF	(LED2 = 1)
#define LED2_TOGGLE (LED2=!LED2)

void LED_Init(void);

#endif
