#ifndef __ADC_H
#define __ADC_H

#include "sys.h"

void ADC_Init(void);
u16 Get_ADC(u8 ch);
u16 Get_ADC_Average(u8 ch,u8 times);

#endif
