#include "adc.h"
#include "delay.h"

//ADC 初始化 ADC1 通道6 PA6
void ADC_Init(void){
	RCC->APB2ENR|=1<<2;		//使能PortA时钟
	GPIOA->CRL&=0xf0ffffff;	//PA6 模拟输入

	RCC->APB2ENR|=1<<9;		//ADC1时钟使能
	RCC->APB2RSTR|=1<<9;	//ADC1复位
	RCC->APB2RSTR&=~(1<<9);	//复位结束
	RCC->CFGR&=~(3<<14);	//ADC分频因子清零
	RCC->CFGR|=2<<14;		//PCLK2 6分频后作为ADC时钟

	ADC1->CR1&=0xf0ffff;	//ADC工作模式清零，独立模式
	// ADC1->CR1|=0<<16;	//独立模式 
	ADC1->CR1&=~(1<<8);		//非扫描模式
	ADC1->CR2&=~(1<<1);		//单次转换模式
	ADC1->CR2&=~(7<<17);	//启动规则通道组转换外部事件配置清零
	ADC1->CR2|=7<<17;		//SWSTART软件控制转换（软件触发）
	ADC1->CR2|=1<<20;		//使用外部事件启动转换
	ADC1->CR2&=~(1<<11);	//数据对齐为右对齐
	ADC1->SQR1&=~(0xf<<20);	//规则通道转换序列中的通道数目配置清零
	// ADC1->SQR1|=0<<20;		//规则通道转换序列中的通道数目设置为1

	ADC1->SMPR2&=~(7<<3);	//通道1采样时间清空
	ADC1->SMPR2|=7<<3;		//通道1采样时间设置为239.5周期,提高采样时间可以提高精确度
	// ADC1->SMPR2&=~(7<<18);	//通道6采样时间清空
	// ADC1->SMPR2|=(7<<18);	//通道6采样时间设置为239.5周期,提高采样时间可以提高精确度

	ADC1->CR2|=1<<0;		//开启ADC并启动转换
	ADC1->CR2|=1<<3;		//复位校准
	while(ADC1->CR2&1<<3);	//等待校准结束
	ADC1->CR2|=1<<2;		//A/D校准
	while(ADC1->CR2&1<<2);	//等待校准结束
}

//获得ADC值 ADC1
u16 Get_ADC(u8 ch){
	ADC1->SQR3&=0xffffffe0;	//规则序列1
	ADC1->SQR3|=ch;			//通道ch
	ADC1->CR2|=1<<22;		//开始转换规则通道
	while(!(ADC1->SR&1<<1));//转换结束位
	return ADC1->DR;		//返回ADC值
}

//获取通道ch的转换值，求times个数据的平均值
u16 Get_ADC_Average(u8 ch,u8 times){
	u32 tempValue =0;
	u8 t;
	for(t=0; t<times; t++){
		tempValue += Get_ADC(ch);
		delay_ms(5);
	}
	return tempValue/times;
} 
