#include "dht11.h"
#include "delay.h"

u8 temperature = 0;	//温度
u8 humidity = 0;	//湿度

//复位DHT11
void DHT11_Rst(void){
	DHT11_IO_OUT();	//SET OUTPUT
	DHT11_DQ_OUT=0;	//拉低DQ
	delay_ms(20);	//拉低至少18ms
	DHT11_DQ_OUT=1;	//DQ=1 
	delay_us(30);	//主机拉高20~40us
}

//检测DHT11的存在(0: 存在; 1: 不存在)
u8 DHT11_Check(void)
{
	u8 retry=0;
	DHT11_IO_IN();	//SET INPUT
	while(DHT11_DQ_IN&&retry<100){	//DHT11会拉低40~80us
		retry++;
		delay_us(1);
	}
	if(retry>=100){
		return 1;
	}else{
		retry=0;
	}
	while(!DHT11_DQ_IN&&retry<100){	//DHT11拉低后会再次拉高40~80us
		retry++;
		delay_us(1);
	}
	if(retry>=100){
		return 1;
	}
	return 0;
}

//从DHT11读取一个位
u8 DHT11_Read_Bit(void)
{
	u8 retry=0;
	while(DHT11_DQ_IN&&retry<100){	//等待变为低电平
		retry++;
		delay_us(1);
	}
	retry=0;
	while(!DHT11_DQ_IN&&retry<100){	//等待变高电平
		retry++;
		delay_us(1);
	}
	delay_us(40);	//等待40us
	if(DHT11_DQ_IN){
		return 1;
	}else{
		return 0;
	}
}

//从DHT11读取一个字节
u8 DHT11_Read_Byte(void)
{
	u8 i, data;
	data=0;
	for(i=0; i<8; i++){
		data<<=1; 
		data|=DHT11_Read_Bit();
	}
	return data;
}

//从DHT11读取一次数据
//temp:温度值(范围:0~50°)
//humi:湿度值(范围:20%~90%)
u8 DHT11_Read_Data(u8 *temp, u8 *humi)
{
	u8 buf[5];
	u8 i;
	DHT11_Rst();
	if(DHT11_Check()==0){
		for(i=0; i<5; i++){	//读取40位数据
			buf[i]=DHT11_Read_Byte();
		}
		if((buf[0]+buf[1]+buf[2]+buf[3])==buf[4]){
			*humi=buf[0];
			*temp=buf[2];
		}
	}else{
		return 1;
	}
	return 0;
}

//初始化DHT11的IO口DQ，同时检测DHT11的存在
u8 DHT11_Init(void)
{
	RCC->APB2ENR|=1<<2;		//使能PortA时钟 
	// GPIOA->CRL&=0xfff0ffff;
	// GPIOA->CRL|=0x00030000;	//PA4通用推挽输出
	DHT11_IO_OUT();
	// GPIOA->ODR|=1<<0;		//输出1
	DHT11_DQ_OUT=1;
	DHT11_Rst();
	return DHT11_Check();
}

//将温度转为字符串
u8 Temperature_To_String(u8 *t, u16 l)
{
	if(l < 3){
		return 1;
	}
	t[0]=temperature / 10 + '0';
	t[1]=temperature % 10 + '0';
	t[2]='\0';
	return 0;
}

//将湿度转为字符串
u8 Humidity_To_String(u8 *h, u16 l)
{
	if(l < 3){
		return 1;
	}
	h[0]=humidity / 10 + '0';
	h[1]=humidity % 10 + '0';
	h[2]='\0';
	return 0;
}

//将温湿度转为字符串
u8 DHT11_To_String(u8 *th, u16 l)
{
	if(l < 12){
		return 1;
	}
	th[0]='T';
	th[1]=th[7]=':';
	th[2]=temperature / 10 + '0';
	th[3]=temperature % 10 + '0';
	th[4]=127;	//℃
	th[5]=' ';
	th[6]='H';
	th[8]=humidity / 10 + '0';
	th[9]=humidity % 10 + '0';
	th[10]='%';
	th[11]='\0';
	return 0;
}
