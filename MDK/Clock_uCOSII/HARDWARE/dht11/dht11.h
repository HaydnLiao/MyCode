#ifndef __DHT11_H
#define __DHT11_H

#include "sys.h"

extern u8 temperature;	//温度
extern u8 humidity;		//湿度

//IO方向设置
#define DHT11_IO_IN()	{GPIOA->CRL&=0xfff0ffff;GPIOA->CRL|=8<<16;}	//上拉/下拉输入模式	PA4
#define DHT11_IO_OUT()	{GPIOA->CRL&=0xfff0ffff;GPIOA->CRL|=3<<16;}	//通用推挽输出模式	PA4
//IO操作函数
#define DHT11_DQ_OUT	PAout(4)	//数据端口	PA4
#define DHT11_DQ_IN		PAin(4)		//数据端口	PA4

void DHT11_Rst(void);
u8 DHT11_Check(void);
u8 DHT11_Read_Bit(void);
u8 DHT11_Read_Byte(void);
u8 DHT11_Read_Data(u8 *temp, u8 *humi);
u8 DHT11_Init(void);

u8 Temperature_To_String(u8 *t, u16 l);
u8 Humidity_To_String(u8 *h, u16 l);
u8 DHT11_To_String(u8 *th, u16 l);

#endif
