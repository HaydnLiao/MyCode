#include "usart.h"
// #include "delay.h"

//重定向c库函数printf到USART1
int fputc(int ch, FILE *f)
{
	while((USART1->SR & 0x40) == 0);	//循环发送,直到发送完毕   
	USART1->DR=(u8)ch;
	return ch;
}

// 重定向c库函数scanf到USART1(存在死机问题 2016.06.03)
// int fgetc(FILE *f)
// {
	// while((USART1->SR & 0x20) == 0);
	// return (USART1->DR);
// }

u8 USART1_RX_BUF[USART1_RX_BUF_SIZE];	//接收缓冲大小
u16 USART1_RX_CNT = 0;
u8 RTC_RESET_FLAG = 0;

//USART1中断函数
void USART1_IRQHandler(void)
{
	u8 res;
	if(USART1->SR & 0x20){	//接收到数据
		res= USART1->DR;
		if(res == 'r' || res == 'R'){
			RTC_RESET_FLAG = 1;
		}
		if(USART1_RX_CNT < USART1_RX_BUF_SIZE) {
			USART1_RX_BUF[USART1_RX_CNT] = res;	//记录接收到的值
			USART1_RX_CNT += 1;					//接收数据增加1 
		} 
	}
}

//初始化IO USART1
//pclk2:PCLK2时钟频率(Mhz)
//bound:波特率
void UART_Init(u32 pclk2, u32 bound)
{
	float temp;
	u16 mantissa;
	u16 fraction;
	temp=(float)(pclk2*1000000)/(bound*16);	//得到USARTDIV
	mantissa=temp;				//得到整数部分
	fraction=(temp-mantissa)*16;	//得到小数部分
	mantissa <<= 4;
	mantissa += fraction; 
	RCC->APB2ENR|=1<<2;		//使能PORTA口时钟  
	RCC->APB2ENR|=1<<14;	//使能USART时钟 
	GPIOA->CRH&=0xFFFFF00F;	//IO状态设置
	GPIOA->CRH|=0x000008B0;	//IO状态设置

	RCC->APB2RSTR|=1<<14;		//复位USART1
	RCC->APB2RSTR&=~(1<<14);	//停止复位

	USART1->BRR=mantissa;	//波特率设置
	USART1->CR1|=0x200C;	//1位停止,无校验位.

	USART1->CR1|=1<<8;	//PE中断使能
	USART1->CR1|=1<<5;	//接收缓冲区非空中断使能
	MY_NVIC_Init(3,3,USART1_IRQn,2);	//组2最低优先级 
}

//USART1发送len个字节.
//buf:发送区首地址
//len:发送的字节数(为了和本代码的接收匹配,这里建议不要超过64个字节)
void USART1_Send_Data(u8 *buf,u16 len)
{
	u16 temp;
	for (temp = 0; temp < len; temp++){		//循环发送数据
		while ((USART1->SR & 0x40) == 0);	//等待发送结束
		USART1->DR = buf[temp];
	}
	while ((USART1->SR & 0x40) == 0);		//等待发送结束
}

//USART1查询接收到的数据
//buf:接收缓存首地址
//len:读到的数据长度
void USART1_Receive_Data(u8 *buf,u16 *len)
{
	u16 rxlen = USART1_RX_CNT;
	u16 temp = 0;
	*len = 0;		//默认为0
	// delay_ms(10);	//等待10ms,连续超过10ms没有接收到一个数据,则认为接收结束
	if(rxlen == USART1_RX_CNT && rxlen){	//接收到了数据,且接收完成了
		for (temp = 0; temp < rxlen; temp++){
			buf[temp] = USART1_RX_BUF[temp];
		}
		*len = USART1_RX_CNT;	//记录本次数据长度
		USART1_RX_CNT = 0;		//清零
	}
}
