#ifndef __USART_H
#define __USART_H

#include "sys.h"
#include <stdio.h>

#define USART1_RX_BUF_SIZE	128	//定义最大接收字节数
extern u8 USART1_RX_BUF[USART1_RX_BUF_SIZE];	//接收缓冲,最大USART_REC_LEN个字节.末字节为换行符 
extern u8 RTC_RESET_FLAG;

int fputc(int ch, FILE *f);
// int fgetc(FILE *f);
void UART_Init(u32 pclk2, u32 bound);
void USART1_Send_Data(u8 *buf, u16 len);
void USART1_Receive_Data(u8 *buf, u16 *len);

#endif
