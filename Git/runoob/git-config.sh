
#! /bin/sh

# 移除旧版本 git
# Centos自带 git，7.x版本自带 git 1.8.3.1（应该是）
yum remove git
# 安装所需软件包
yum install curl-devel expat-devel gettext-devel openssl-devel zlib-devel 
yum install gcc-c++ perl-ExtUtils-MakeMaker
# 下载 git
cd /usr/src
wget https://www.kernel.org/pub/software/scm/git/git-2.7.3.tar.gz
# 解压
tar xf git-2.7.3.tar.gz
# 配置编译安装（根据实际目录修改gitPath）
cd git-2.7.3
make configure
./configure --prefix=gitPath ##配置目录
make profix=gitPath
make install
# 加入环境变量
echo "export PATH=$PATH:gitPath" >> /etc/profile
source /etc/profile
# 检查版本
git --version 

# git 提供了一个叫做 git config 的工具，专门用来配置或读取相应的工作环境变量。
# /etc/gitconfig 文件：系统中对所有用户都普遍适用的配置。
# 若使用 git config 时用 --system 选项，读写的就是这个文件。
# ~/.gitconfig 文件：用户目录下的配置文件只适用于该用户。
# 若使用 git config 时用 --global 选项，读写的就是这个文件。
# 当前项目的 Git 目录中的配置文件（也就是工作目录中的 .git/config 文件）：这里的配置仅仅针对当前项目有效。
# 每一个级别的配置都会覆盖上层的相同配置，所以 .git/config 里的配置会覆盖 /etc/gitconfig 中的同名变量。

# 配置个人的用户名称和电子邮件地址
git config --global usr.name "HaydnLiao"
git config --global user.email HaydnLiao@163.com

# 默认使用的文本编辑器（默认为vi或vim）
git config --global core.editor vim

# 差异分析工具
git config --global merge.tool vimdiff

# 查看配置信息
git config --list

# git 一般工作流程如下：
# 克隆 git 资源作为工作目录。
# 在克隆的资源上添加修改文件或者更新他人的修改。
# 在提交前查看修改并提交修改。
# 修改完成后发现错误可以撤回提交并再次修改再提交。
# 每日工作后将提交的修改推送到远端仓库。

# 工作区：电脑里能看到的目录。
# 暂存区：一般存放在.git/index文件中。英文叫stage或index。
# 版本库：工作区里的一个隐藏目录.git.

# "HEAD" 实际是指向 master 分支的一个"游标"。
# Git 的对象库位于 ".git/objects" 目录下，包含了创建的各种对象及内容。
# "git add": 暂存区的目录树被更新，工作区修改或新增的内容被写入到对象库中一个新的对象中，该对象的ID被记录在暂存区的文件索引中。
# "git commit": 暂存区的目录树写到版本库（对象库）中，master 指向提交时暂存区的目录树。
# "git reset HEAD": 暂存区的目录树会被重写，被 master 分支指向的目录树所替换，但是工作区不受影响。
# "git rm --cached <file>": 会直接从暂存区删除文件，工作区则不做出改变。
# "git checkout ." 或者 "git checkout -- <file>": 用暂存区全部或指定的文件替换工作区的文件，会清除工作区中未添加到暂存区的改动。
# "git checkout HEAD ." 或者 "git checkout HEAD <file>": 用 HEAD 指向的 master 分支中的全部或者部分文件替换暂存区和以及工作区中的文件,清除工作区和暂存区中未提交的改动.

# 初始化仓库,生成隐藏目录.git
git init ~/git-local
# 克隆远端仓库到当前目录
git clone https://github.com/HaydnLiao/MyCode.git 
# 添加修改或解决冲突
git add *|file
# 查看状态
git status [-s]
# 工作区尚未缓存的改动：git diff
# 暂存区已缓存的改动： git diff --cached
# 已缓存的与未缓存的所有改动：git diff HEAD
# 显示摘要而非整个 diff：git diff --stat
git commit [-m ""]
# 取消已缓存的内容
git reset HEAD [file]
# 从工作区和暂存区删除文件：git rm file
# 删除暂存区的文件：git rm --cached
# 重命名
git mv nameOri nameDes

# 显示所有分支
git branch
# 创建分支
git branch branchName
# 切换分支
git checkout branchName
# 创建并切换分支
git checkout -b branchName
# 删除分支
git branch -d branchName
# 合并分支
git merge branchOne branchTwo ...

# 查看提交历史
git log [--oneline] [--graph | --reverse] 
# 按作者查找：--author authorName
# 指定日期：--since, --before, --until, --after timePara(3.weeks.ago, 2010-04-18)
# 隐藏合并提交：--no-merges

# 带注解的标签(版本库的快照)
git tag -a tagName [hashNumber]
# 查看标签
git tag
# 删除标签
git tag -d tagName
# 查看标签版本修改内容
git show tagName

# ssh方式添加远程仓库
ssh-keygen -t rsa -C "xxx@xxx.com"
# 将~/.ssh/id_rsa.pub文件里的key复制到剪贴板，添加到远程仓库设置中
clip < ~/.ssh/id_rsa.pub
# 提交到远程仓库
git remote add origin|otherName git@github.com:HaydnLiao/MyCode.git
git push [origin|otherName] [master|otherName] 

# 查看远程库
git remote [-v]
# 从远程仓库下载新分支与数据
git fetch origin|otherName
# 从远程仓库提取数据并尝试合并到当前分支
git pull origin|otherName
# 将分支推送到远程仓库同名分支
git push [origin|otherName] [master|otherName] 
# 删除本地远程仓库链接
git remote rm origin|otherName

# Git服务器搭建（ssh方式）
# 安装Git，创建名为git的用户组和用户
useradd -c "git server account" -m -r -U git  
[passed git]
# 将开发者的SSH公钥导入/home/git/.ssh/authorized_keys
su git
ssh-keygen -t rsa -f ~/.ssh/git_rsa -C "xxx@xxx" -b 4096  
cd ~/.ssh/
cat git_rsa.pub >> authorized_keys
# 若克隆仓库需要密码，
# 查看开发者的ssh公钥是否为一行；
# 更改服务器端用户文件权限；
# 服务器用户$HOME目录权限不能有用户组写权限。
chmod 700 ~
# 服务器用户$HOME/.ssh目录权限必须是700
chmod 700 ~/.ssh
# 服务器用户$HOME/.ssh/authorized_keys文件权限必须是600
chmod 600 ~/.ssh/authorized_keys
# 初始化Git仓库
git init --bare xxx.git
# 修改/etc/passwd禁用git用户通过shell登录
su root
git:x:986:981:git server account:/home/git:/usr/bin/git-shell
# 克隆仓库
git clone git@192.168.235.128:xxx.git
git clone git@192.168.235.128:/home/git/xxx.git


