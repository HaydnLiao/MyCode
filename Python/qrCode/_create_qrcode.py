# -*- coding: utf-8 -*-
# filename: _create_qrcode.py

# pip install qrcode
# pip install pillow

import qrcode
from PIL import Image

data = 'www.HaydnLiao.top'
img_file = r'py_qrcode.png'

# img = qrcode.make(data)
# # 图片数据保存至本地文件
# img.save(img_file)
# # 显示二维码图片
# img.show()

qr = qrcode.QRCode(
    version=None,
    error_correction=qrcode.constants.ERROR_CORRECT_H,
    box_size=20,
    border=4,
)
# version: 一个整数，范围为1到40，表示二维码的大小（最小值是1，是个12×12的矩阵），如果想让程序自动生成，将值设置为 None 并使用 fit=True 参数即可。
# error_correction: 二维码的纠错范围，可以选择4个常量：
#     ERROR_CORRECT_L 7%以下的错误会被纠正
#     ERROR_CORRECT_M (default) 15%以下的错误会被纠正
#     ERROR_CORRECT_Q 25 %以下的错误会被纠正
#     ERROR_CORRECT_H. 30%以下的错误会被纠正
# boxsize: 每个点（方块）中的像素个数
# border: 二维码距图像外围边框距离，默认为4，而且相关规定最小为4
qr.add_data(data)
qr.make(fit=True)
img = qr.make_image()
img = img.convert("RGBA")
icon = Image.open("HaydnLiao.png")
img_w, img_h = img.size
icon_w = int(img_w / 4)
icon_h = int(img_h / 4)
icon = icon.resize((icon_w, icon_h), Image.ANTIALIAS)
w = int((img_w - icon_w) / 2)
h = int((img_h - icon_h) / 2)
img.paste(icon, (w, h), icon)
img.save(img_file)
