# -*- coding: utf-8 -*-
# filename: main.py
import web
from handle import Handle
from static import Static

urls = (
    '/', 'Handle',
    '/(\w+\.png)', 'Static',
    # '/favicon.ico', 'Icon'
)

if __name__ == '__main__':
    app = web.application(urls, globals())
    app.run()

