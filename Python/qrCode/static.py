# -*- coding: utf-8 -*-
# filename: static.py

import web

class Static(object):
    def GET(self, fp):
        # print "<Static>", fp
        try:
            with open(fp, "rb") as fs:
                return fs.read()
        except Exception, err:
            return ">>>Static>>>", err
