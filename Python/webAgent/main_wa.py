# -*- coding: utf-8 -*-
# filename: main.py
import web
from handle_wa import Handle
from static_wa import Static

urls = (
    '/', 'Handle',
    # TODO:可以直接将页面请求的文件直接转发
    '/([_\w]+/[\u4e00-\u9fa5 | -_=,&\w]+\.\w+)', 'Static'
    # '/favicon.ico', 'Icon'
)

if __name__ == '__main__':
    app = web.application(urls, globals())
    app.run()

