# -*- coding: utf-8 -*-
# filename: static_wa.py

import web

class Static(object):
    def GET(self, fp):
        print "<Static>", fp
        if '?website=' in fp:
            web.seeother('/?{}'.format(fp.split('?')[-1]))
        else:
            try:
                with open(fp, "rb") as fs:
                    return fs.read()
            except Exception, err:
                # TODO: 可以加入404页面
                return ">>>Static>>>", err
