
#!/usr/bin/python3 #linux head
#!/usr/bin/env python3

print("Hello, Python!")
print()

'''
以下划线开头的标识符是有特殊意义的.
以单下划线开头 _foo 的代表不能直接访问的类属性,需通过类提供的接口进行访问,不能用 from xxx import * 而导入;
以双下划线开头的 __foo 代表类的私有成员;
以双下划线开头和结尾的 __foo__ 代表 Python 里特殊方法专用的标识,如 __init__() 代表类的构造函数.
Python 可以同一行显示多条语句,方法是用分号 ; 分开.
'''
import keyword
print(keyword.kwlist)

if True:
	print("True")
else:
	print("Faulse")
print()

paragraph = """1 This is a paragraph,
2 it has two lines."""
"""
这是多行注释,使用双引号.
这是多行注释,使用双引号.
这是多行注释,使用双引号.
"""
print(paragraph, end="") #不换行输出
print("end=''")
#print("The character string:", input("Please enter character string:"))
#input("Press the enter key to exit.")
#自然字符串, 通过在字符串前加r或R,如 r"this is a line with \n" 则\n会显示,并不是换行.
#python允许处理unicode字符串,加前缀u或U, 如 u"this is an unicode string".
print(r"this is a line with \n")
print()

'''
在 python 用 import 或者 from...import 来导入相应的模块.
将整个模块(somemodule)导入,格式为: import somemodule
从某个模块中导入某个函数,格式为: from somemodule import somefunction
从某个模块中导入多个函数,格式为: from somemodule import firstfunc, secondfunc, thirdfunc
将某个模块中的全部函数导入,格式为: from somemodule import *
'''
import sys
print('================Python import mode==========================');
print("命令行参数为:", end='')
for i in sys.argv:
    print(i)
print("Python 路径为", sys.path)

from sys import argv,path #导入特定的成员
print('================Python from import mode===================================')
print("Python 路径为", path) 
#因为已经导入path成员,所以此处引用时不需要加sys.path
print()

'''
Python有五个标准的数据类型:
	Numbers（数字）
		int（长整型）
		float（浮点型）
		bool（布尔型）
		complex（复数）
	String（字符串）
		从左到右索引默认0开始的,最大范围是字符串长度少1
		从右到左索引默认-1开始的,最大范围是字符串开头
		加号（+）是字符串连接运算符,星号（*）是重复操作
	List（列表）
		与String相似
	Tuple（元组）
		与List相似
		用"()"标识,不能二次赋值,相当于只读列表
		string、list和tuple都属于sequence（序列）
		序列都可以进行切片、加、乘、检查成员
	Sets（集合）
		无序不重复元素的序列,基本功能是进行成员关系测试和删除重复元素
		可以使用大括号 { } 或者 set() 函数创建集合
		创建一个空集合必须用 set() 而不是 { },因为 { } 是用来创建一个空字典
	Dictionary（字典）
		无序的对象集合,用"{ }"标识
		通过键来存取,由索引(key)和它对应的值(value)组成
		键(key)必须使用不可变类型且不能重复
'''
var1=123
var2=5.20
var3=True
var4=4.53e-7j
var5=complex(4.53e-7,0)
print(var1,var2,var3,var4,var5)
print(type(var1),type(var2),type(var3),type(var4),type(var5))
print("isinstance(var1,int):", isinstance(var1,int))
print()

'''
isinstance()会认为子类是一种父类类型.
type()不会认为子类是一种父类类型.
'''
class A:
    pass

class B(A):
    pass

isinstance(A(), A)  # returns True
type(A()) == A      # returns True
isinstance(B(), A)  # returns True
type(B()) == A      # returns False
print()

str="Hello World!"
print(("str="+str+";")*2)
print()

list=['Haydn', 123, 3.14, 'Liao', 465]
print((list+['add','list'])*2)
print()

tuple=('Haydn', 123, 3.14, 'Liao', 465)
print((tuple+('add','tuple'))*2)
print()

sets={'one', 'two', 'three', 'one', 'two'}
print("set={'one', 'two', 'three', 'one', 'two'}")
print(sets)
# 成员测试
if('Rose' in sets) :
    print('Rose in the set')
else :
    print('Rose don\'t in the set')
#set可以进行集合运算
a = set('abracadabra')
b = set('alacazam')
print(a,b)
print(a - b)     # a和b的差集
print(a | b)     # a和b的并集
print(a & b)     # a和b的交集
print(a ^ b)     # a和b中不同时存在的元素
print()

dictT={}
dictT['one']="This is one!"
dictT[2]="This is 2!"
print(dictT['one'],dictT[2])
print(dictT)
print(dictT.keys())
print(dictT.values())
print(dict([('Runoob',1), ('Google',2), ('Taobao',3)]))
print({x : x**2 for x in (2, 4, 6)})
print(dict(Runoob=1, Google=2, Taobao=3))
print()

'''
数据类型转换
	int(x [,base])			将x转换为一个整数
	long(x [,base] )		将x转换为一个长整数
	float(x)				将x转换到一个浮点数
	complex(real [,imag])	创建一个复数
	str(x)					将对象 x 转换为字符串
	repr(x)					将对象 x 转换为表达式字符串
	eval(str)				用来计算在字符串中的有效Python表达式,并返回一个对象
	tuple(s)				将序列 s 转换为一个元组
	list(s)					将序列 s 转换为一个列表
	set(s)					转换为可变集合
	dict(d)					创建一个字典.d 必须是一个序列 (key,value)元组.
	frozenset(s)			转换为不可变集合
	chr(x)					将一个整数转换为一个字符
	unichr(x)				将一个整数转换为Unicode字符
	ord(x)					将一个字符转换为它的整数值
	hex(x)					将一个整数转换为一个十六进制字符串
	oct(x)					将一个整数转换为一个八进制字符串
'''

'''
运算符
	算术运算符: + - * / % **(幂) //(整除)
	比较（关系）运算符: == !=  > < >= <=
	赋值运算符: = += -= *= /= %= **= //=
	位运算符: & | ~ ^ << >>
	逻辑运算符: and,or,not
	成员运算符: in,not in
	身份运算符: is,is not
'''
'''
数值运算
	1、Python可以同时为多个变量赋值,如a, b = 1, 2.
	2、一个变量可以通过赋值指向不同类型的对象.
	3、数值的除法（/）总是返回一个浮点数,要获取整数使用//操作符.
	4、在混合计算时,Python会把整型转换成为浮点数.
'''
print("2**10 =", 2**10, " 10//3 =", 10//3, " 10/3 =", 10/3)
print("1!=2:",1!=2)
a='Haydn'
b='Liao'
listT=['Haydn','Liao','haydn','liao']
print(a in listT, b not in listT)
print()

#id()函数用于获取对象内存地址
#is用于判断两个变量引用对象是否为同一个, == 用于判断引用变量的值是否相等.
a=[1,2,3]
b=a
print("b=a ", a, b, a is b, id(a), id(b), id(a) is id(b))
b=[1,2,3]
print("b=[1,2,3] ", a, b, a is b, id(a), id(b), id(a) is id(b))
print()

'''
运算符优先级
	**			指数 (最高优先级)
	~ + -		按位翻转, 一元加号和减号 (最后两个的方法名为 +@ 和 -@)
	* / % //	乘,除,取模和取整除
	+ -			加法减法
	>> <<		右移,左移运算符
	&			位 'AND'
	^ |			位运算符
	<= < > >=	比较运算符
	<> == !=	等于运算符
	= %= /= //= -= += *= **=	赋值运算符
	is,is not	身份运算符
	in,not in	成员运算符
	not,or,and	逻辑运算符
'''
'''
数学函数
	abs(x)		返回数字的绝对值,如abs(-10) 返回 10
	ceil(x)		返回数字的上入整数,如math.ceil(4.1) 返回 5
	cmp(x, y)	如果 x < y 返回 -1, 如果 x == y 返回 0, 如果 x > y 返回 1. Python 3 已废弃 .使用 使用 (x>y)-(x<y) 替换.
	exp(x)		返回e的x次幂(ex),如math.exp(1) 返回2.718281828459045
	fabs(x)		返回数字的绝对值,如math.fabs(-10) 返回10.0
	floor(x)	返回数字的下舍整数,如math.floor(4.9)返回 4
	log(x)		如math.log(math.e)返回1.0,math.log(100,10)返回2.0
	log10(x)	返回以10为基数的x的对数,如math.log10(100)返回 2.0
	max(x1, x2,...)	返回给定参数的最大值,参数可以为序列.
	min(x1, x2,...)	返回给定参数的最小值,参数可以为序列.
	modf(x)		返回x的整数部分与小数部分,两部分的数值符号与x相同,整数部分以浮点型表示.
	pow(x, y)	x**y 运算后的值.
	round(x [,n])	返回浮点数x的四舍五入值,如给出n值,则代表舍入到小数点后的位数.
	sqrt(x)	返回数字x的平方根,数字可以为负数,返回类型为实数,如math.sqrt(4)返回 2+0j
'''
'''
随机数函数
	choice(seq)		从序列的元素中随机挑选一个元素,比如random.choice(range(10)),从0到9中随机挑选一个整数.
	randrange ([start,] stop [,step])	从指定范围内,按指定基数递增的集合中获取一个随机数,基数缺省值为1
	random()		随机生成下一个实数,它在[0,1)范围内.
	seed([x])		改变随机数生成器的种子seed.如果你不了解其原理,你不必特别去设定seed,Python会帮你选择seed.
	shuffle(lst)	将序列的所有元素随机排序
	uniform(x, y)	随机生成下一个实数,它在[x,y]范围内.
'''
'''
三角函数
	acos(x)		返回x的反余弦弧度值.
	asin(x)		返回x的反正弦弧度值.
	atan(x)		返回x的反正切弧度值.
	atan2(y, x)	返回给定的 X 及 Y 坐标值的反正切值.
	cos(x)		返回x的弧度的余弦值.
	hypot(x, y)	返回欧几里德范数 sqrt(x*x + y*y).
	sin(x)		返回的x弧度的正弦值.
	tan(x)		返回x弧度的正切值.
	degrees(x)	将弧度转换为角度,如degrees(math.pi/2),返回90.0
	radians(x)	将角度转换为弧度
'''
'''
数学常量 math
	pi	数学常量 pi（圆周率,一般以π来表示）
	e	数学常量 e,e即自然常数（自然常数）
'''

#符号%用来格式化字符串
print ("My name is %s. I am %d years old!" % ("HaydnLiao", 23))
print("%#o %#x" %(123, 456))
print()
'''
字符串常用内建函数 xxx(字符串名).xxx(函数名)
	1	capitalize()				将字符串的第一个字符转换为大写
	2	center(width, fillchar)		返回一个指定的宽度 width 居中的字符串,fillchar 为填充的字符,默认为空格.
	3	count(str, beg=0, end=len(string))				返回 str 在 string 里面出现的次数,如果 beg 或者 end 指定则返回指定范围内 str 出现的次数
	4	bytes.decode(encoding="utf-8", errors="strict")	Python3 中没有 decode 方法,但我们可以使用 bytes 对象的 decode() 方法来解码给定的 bytes 对象,这个 bytes 对象可以由 str.encode() 来编码返回
	5	encode(encoding='UTF-8', errors='strict')		以 encoding 指定的编码格式编码字符串,如果出错默认报一个ValueError 的异常,除非 errors 指定的是'ignore'或者'replace'
	6	endswith(suffix, beg=0, end=len(string))		检查字符串是否以 suffix 结束,如果beg 或者 end 指定则检查指定的范围内是否以 obj 结束,如果是,返回 True,否则返回 False
	7	expandtabs(tabsize=8)		把字符串 string 中的 tab 符号转为空格,tab 符号默认的空格数是 8
	8	find(str, beg=0,end=len(string))				检测 str 是否包含在字符串中 中,如果 beg 和 end 指定范围,则检查是否包含在指定范围内,如果是返回开始的索引值,否则返回-1
	9	index(str, beg=0, end=len(string))				跟find()方法一样,只不过如果str不在字符串中会报一个异常
	10	isalnum()					如果字符串至少有一个字符并且所有字符都是字母或数字则返回 True,否则返回 False
	11	isalpha()					如果字符串至少有一个字符并且所有字符都是字母则返回 True, 否则返回 False
	12	isdigit()					如果字符串只包含数字则返回 True 否则返回 False
	13	islower()					如果字符串中包含至少一个区分大小写的字符,并且所有这些(区分大小写的)字符都是小写,则返回 True,否则返回 False
	14	isnumeric()					如果字符串中只包含数字字符,则返回 True,否则返回 False
	15	isspace()					如果字符串中只包含空格,则返回 True,否则返回 False
	16	istitle()					如果字符串是标题化的(见 title())则返回 True,否则返回 False
	17	isupper()					如果字符串中包含至少一个区分大小写的字符,并且所有这些(区分大小写的)字符都是大写,则返回 True,否则返回 False
	18	join(seq)					以指定字符串作为分隔符,将 seq 中所有的元素(的字符串表示)合并为一个新的字符串
	19	len(string)					返回字符串长度
	20	ljust(width[, fillchar])	返回一个原字符串左对齐,并使用 fillchar 填充至长度 width 的新字符串,fillchar 默认为空格.
	21	lower()						转换字符串中所有大写字符为小写.
	22	lstrip()					截掉字符串左边的空格
	23	maketrans()					创建字符映射的转换表,对于接受两个参数的最简单的调用方式,第一个参数是字符串,表示需要转换的字符,第二个参数也是字符串表示转换的目标.
	24	max(str)					返回字符串 str 中最大的字母.
	25	min(str)					返回字符串 str 中最小的字母.
	26	replace(old, new [, max])	将字符串中的 str1 替换成 str2,如果 max 指定,则替换不超过 max 次.
	27	rfind(str, beg=0,end=len(string))				类似于 find()函数,不过是从右边开始查找.
	28	rindex( str, beg=0, end=len(string))			类似于 index(),不过是从右边开始.
	29	rjust(width,[, fillchar])	返回一个原字符串右对齐,并使用fillchar(默认空格）填充至长度 width 的新字符串
	30	rstrip()					删除字符串字符串末尾的空格.
	31	split(str="", num=string.count(str))			num=string.count(str)) 以 str 为分隔符截取字符串,如果 num 有指定值,则仅截取 num 个子字符串
	32	splitlines([keepends])		按照行('\r', '\r\n', \n')分隔,返回一个包含各行作为元素的列表,如果参数 keepends 为 False,不包含换行符,如果为 True,则保留换行符.
	33	startswith(str, beg=0,end=len(string))			检查字符串是否是以 obj 开头,是则返回 True,否则返回 False.如果beg 和 end 指定值,则在指定范围内检查.
	34	strip([chars])				在字符串上执行 lstrip()和 rstrip()
	35	swapcase()					将字符串中大写转换为小写,小写转换为大写
	36	title()						返回"标题化"的字符串,就是说所有单词都是以大写开始,其余字母均为小写(见 istitle())
	37	translate(table, deletechars="")				根据 str 给出的表(包含 256 个字符)转换 string 的字符, 要过滤掉的字符放到 deletechars 参数中
	38	upper()						转换字符串中的小写字母为大写
	39	zfill (width)				返回长度为 width 的字符串,原字符串右对齐,前面填充0
	40	isdecimal()					检查字符串是否只包含十进制字符,如果是返回 True,否则返回 False
'''

a="abcdefghijklmnopqrstuvwxyz"
b="0ar1cs2et3gu4iv5kw6mx7o8q9"
c=str.maketrans(a,b)
d="yesterday once more"
print(a, "=>", a.translate(c))
print(d, "=>", d.translate(c))
print()

'''
List（列表）常用方法
1	list.append(obj)		在列表末尾添加新的对象
2	list.count(obj)			统计某个元素在列表中出现的次数
3	list.extend(seq)		在列表末尾一次性追加另一个序列中的多个值（用新列表扩展原来的列表）
4	list.index(obj)从列表中找出某个值第一个匹配项的索引位置
5	list.insert(index, obj)	将对象插入列表
6	list.pop(obj=list[-1])	移除列表中的一个元素（默认最后一个元素）,并且返回该元素的值
7	list.remove(obj)		移除列表中某个值的第一个匹配项
8	list.reverse()			反向列表中元素
9	list.sort([func])		对原列表进行排序
10	list.clear()			清空列表
11	list.copy()				复制列表
'''
#创建二维列表
#list_2d = [[0 for col in range(cols)] for row in range(rows)]
list_2d = [[0 for i in range(5)] for i in range(6)]
print(list_2d)
print()

tup1 = (50,); #元组中只包含一个元素时,需要在元素后面添加逗号

'''
Dictionary（字典）常用方法
	1	dict.clear()		删除字典内所有元素
	2	dict.copy()		返回一个字典的浅复制
	3	dict.fromkeys()	创建一个新字典,以序列seq中元素做字典的键,val为字典所有键对应的初始值
	4	dict.get(key, default=None)			返回指定键的值,如果值不在字典中返回default值
	5	key in dict				如果键在字典dict里返回true,否则返回false
	6	dict.items()		以列表返回可遍历的(键, 值) 元组数组
	7	dict.keys()		以列表返回一个字典所有的键
	8	dict.setdefault(key, default=None)	和get()类似, 但如果键不存在于字典中,将会添加键并将值设为default
	9	dict.update(dict2)					把字典dict2的键/值对更新到dict里
	10	dict.values()	以列表返回字典中的所有值
	11	dict.pop(key[,default])		删除字典给定键 key 所对应的值,返回值为被删除的值.key值必须给出,否则,返回default值
	12	dict.popitem()				随机返回并删除字典中的一对键和值(打印时排在最后的一对)
'''

d = {x : x**2 for x in range(1,20,2)}
print(d)
print(d.popitem())
print()

input("Press the enter key to exit.")
