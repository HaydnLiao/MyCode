
# 列表-堆栈
print("===stack===")
stack = [3, 4, 5]
print(stack)
stack.append(6)
stack.append(7)
print(stack)
stack.pop()
print(stack)
stack.pop()
print(stack)
print()

# 列表-队列
print("===queue===")
queue = ["Eric", "John", "Michael"]
print(queue)
queue.append("Terry")
queue.append("Graham")
print(queue)
queue.pop(0)
print(queue)
queue.pop(0)
print(queue)
print()

# 列表推导式
'''
列表推导式提供了从序列创建列表的简单途径.通常应用程序将一些操作应用于某个序列的每个元素,用其获得的结果作为生成新列表的元素,或者根据确定的判定条件创建子序列.
每个列表推导式都在 for 之后跟一个表达式,然后有零到多个 for 或 if 子句.返回结果是一个根据表达从其后的 for 和 if 上下文环境中生成出来的列表.
如果希望表达式推导出一个元组,就必须使用括号.
'''
vec = [2, 4, 6]
listV = [3*x for x in vec]
print(listV)
listV = [[x, x**2] for x in vec]
print(listV)
listV = [x**3 for x in vec if x > 3]
print(listV) 
listV = [[x+y**2 for x in vec for y in vec]]
print(listV)
listV = [vec[i//2] for i in vec if i/2 < 3]
print(listV)
print()

# 嵌套列表解析
matrix = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
print(matrix)
# 矩阵转置
matrixT = [[row[i] for row in matrix] for i in range(len(matrix[0]))]
print(matrixT)
print()

# 集合
a = {x for x in 'abracadabra' if x not in 'abc'}
print(a)
print()

# 字典
dictT = dict([('sape', 4139), ('guido', 4127), ('jack', 4098)])
print(dictT)
dictT = {x: x**2 for x in range(1, 5)}
print(dictT)
# 关键字只是简单的字符串
dictT = dict(sape=4139, guido=4127, jack=4098)
print(dictT)
print()

# 遍历-序列-单个
for i, v in enumerate(['tic', 'tac', 'toe']):
	print('[', i, v, ']', end = ' ')
print()
# 遍历-序列-两个或多个
questions = ['name', 'quest', 'favorite color']
answers = ['lancelot', 'the holy grail', 'blue']
for q, a in zip(questions, answers):
	print("What is your {0}?  It is {1}.".format(q, a))
# 遍历-序列-反向
for i in reversed(['one', 'two', 'three']):
	print(i, end = ' ')
print()
# 遍历-序列-排序
for i in sorted(set(['banana', 'apple', 'dog', 'cat'])):
	print(i, end = ' ')
print('\n')

# 遍历-字典
knights = {'gallahad':'the pure', 'robin':'the brave'}
for k, v in knights.items():
	print(k, v)
