
# 迭代器 iter() next()
listT = list(range(10))
print(listT)
it = iter(listT)
while True:
	try:
		print(next(it), end=',')
	except StopIteration:
		print("StopIteration!")
		break
print()

# 生成器 yield
# 生成器是一个返回迭代器的函数
def yieldTest(n):
	cnt = 1
	while(cnt <= n):
		yield cnt**2
		cnt += 1

sqrtN = yieldTest(100)
while True:
	try:
		print(next(sqrtN), end=",")
	except StopIteration:
		print("StopIteration!")
		break
