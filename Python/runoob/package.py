
# 包是一种管理 Python 模块命名空间的形式,采用"点模块名称".
# 比如一个模块的名称是 A.B, 那么他表示一个包 A中的子模块 B.
# 目录只有包含一个叫做 __init__.py 的文件才会被认作是一个包.
print("__name__:", __name__)
# import语法会首先把item当作一个包定义的名称,如果没找到,再试图按照一个模块去导入.如果还没找到,则抛出异常ImportError.
# 反之,如果使用形如import item.subitem.subsubitem这种导入形式,除了最后一项,都必须是包,而最后一项则可以是模块或者是包,但是不可以是类、函数或者变量的名字.

