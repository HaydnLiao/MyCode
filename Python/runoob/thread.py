
'''
每个线程都有一组CPU寄存器,称为线程的上下文,反映了该线程上次运行的状态,最重要的为指令指针和堆栈指针寄存器.
线程不能够独立执行,总是在进程中得到上下文中运行的.每个独立的线程有一个程序运行的入口、顺序执行序列和程序的出口.
线程可以被抢占（中断）.在其他线程正在运行时,线程可以暂时搁置（睡眠）,也就是线程的退让.
'''
'''
线程可以分为:
内核线程：由操作系统内核创建和撤销.
用户线程：不需要内核支持而在用户程序中实现的线程.
线程中常用的两个模块为：_threa、threading(推荐使用)
使用线程有两种方式：函数或者用类来包装线程对象。
'''
'''
调用 _thread 模块中的start_new_thread()函数来产生新线程。
_thread.start_new_thread ( function, args[, kwargs] )
function - 线程函数。
args - 传递给线程函数的参数,他必须是个tuple类型。
kwargs - 可选参数。
'''
'''
# 使用 _thread 模块 - 函数实现
import _thread
import time
# 为线程定义一个函数
def print_time(threadName, delay):
   count = 0
   while count < 5:
      time.sleep(delay)
      count += 1
      print ("%s: %s" % ( threadName, time.ctime(time.time()) ))
# 创建两个线程
try:
   _thread.start_new_thread( print_time, ("Thread-1", 2, ) )
   _thread.start_new_thread( print_time, ("Thread-2", 4, ) )
except:
   print ("Error: Unable to start a thread!")
input()
'''

'''
_thread 提供了低级别的、原始的线程以及一个简单的锁，它相比于 threading 模块的功能还是比较有限的。
threading 模块除了包含 _thread 模块中的所有方法外，还提供的其他方法：
    threading.currentThread(): 返回当前的线程变量。
    threading.enumerate(): 返回一个包含正在运行的线程的list。正在运行指线程启动后、结束前，不包括启动前和终止后的线程。
    threading.activeCount(): 返回正在运行的线程数量，与len(threading.enumerate())有相同的结果。
除了使用方法外，线程模块同样提供了Thread类来处理线程，Thread类提供了以下方法:
run(): 用以表示线程活动的方法。
start():启动线程活动。
join([time]): 等待至线程中止。阻塞调用线程直至线程的join()方法被调用中止、线程正常退出或者抛出未处理的异常，或者是可选的超时发生。
isAlive(): 返回线程是否活动的。
getName(): 返回线程名。
setName(): 设置线程名。
'''
'''
# 使用 threading 模块创建线程 - 类包装线程对象
import threading
import time
exitFlag = 0
class MyThread(threading.Thread):
    def __init__(self, threadID, name, delay):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.delay = delay
    def run(self):
        print ("thread start:" + self.name)
        print_time(self.name, self.delay, 5)
        print ("thread exit:" + self.name)
def print_time(threadName, delay, counter):
    while counter:
        if exitFlag:
            threadName.exit()
        time.sleep(delay)
        print ("%s: %s" % (threadName, time.ctime(time.time())))
        counter -= 1
# 创建新线程
thread1 = MyThread(1, "Thread-1", 2)
thread2 = MyThread(2, "Thread-2", 3)
# 开启新线程
thread1.start()
thread2.start()
thread1.join()
thread2.join()
print("Main process has exited!")
'''

# 线程同步
'''
如果多个线程共同对某个数据修改，则可能出现不可预料的结果，为了保证数据的正确性，需要对多个线程进行同步。
使用 Thread 对象的 Lock 和 Rlock 可以实现简单的线程同步，这两个对象都有 acquire 方法和 release 方法。
'''
'''
import threading
import time
class MyThread (threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
    def run(self):
        print ("Thread start:" + self.name)
        print_or_change_num(self.threadID)
        print ("thread exit:" + self.name)

def print_or_change_num(threadID):
    while True:
        if(threadID > 0):
            # 获取锁，用于线程同步
            threadLock.acquire()
            print("{}:{}".format(threadID, numList))
            # 释放锁，开启下一个线程
            threadLock.release()
            time.sleep(1)
        else:
            # 获取锁，用于线程同步
            threadLock.acquire()
            for i in range(len(numList)):
                print("{}:{}".format(threadID, i))
                numList[i] = ~numList[i]
                time.sleep(1)
            # 释放锁，开启下一个线程
            threadLock.release()
            time.sleep(0.1)

numList = [0 for i in range(10)]
threadLock = threading.Lock()
threads = []
cntThread = 0
MaxThreadAmount = 5
# 创建新线程
while cntThread < MaxThreadAmount:
    threads.append(MyThread(cntThread, "Thread-{}".format(cntThread)))
    cntThread += 1
# 开启新线程
for t in threads:
    t.start()
# 等待所有线程完成
for t in threads:
    t.join()
print("Main process has exited!")
'''

# 线程优先级队列(queue)
'''
Queue 模块中提供了同步的、线程安全的队列类,FIFO 队列 Queue，LIFO 队列 LifoQueue 和优先级队列 PriorityQueue。
这些队列都实现了锁原语，能够在多线程中直接使用，可以使用队列来实现线程间的同步。
'''
'''
Queue 模块中的常用方法:
Queue.qsize()   返回队列的大小
Queue.empty()   如果队列为空，返回True,反之False
Queue.full()    如果队列满了，返回True,反之False
Queue.full      与 maxsize 大小对应
Queue.get([block[, timeout]])   获取队列，timeout等待时间
Queue.get_nowait()              相当Queue.get(False)
Queue.put(item)                 写入队列，timeout等待时间
Queue.put_nowait(item)          相当Queue.put(item, False)
Queue.task_done()               在完成一项工作之后，Queue.task_done()函数向任务已经完成的队列发送一个信号
Queue.join()                    实际上意味着等到队列为空，再执行别的操作
'''
import queue
import threading
import time
exitFlag = 0
class MyThread (threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
    def run(self):
        print ("Thread start:" + self.name)
        print_thread_data(self.threadID)
        print ("thread exit:" + self.name)

def print_thread_data(threadID):
    while not exitFlag:
        if not workQueue.empty():
            data = workQueue.get()
            print ("[{}]{}".format(threadID, data))
        time.sleep(1)

threads = []
MaxThreadAmount = 3
MaxQueueAmount = 10
workQueue = queue.Queue(10)
# 创建新线程,填充队列
for cnt in range(MaxThreadAmount):
    threads.append(MyThread(cnt, "Thread-{}".format(cnt)))
for cnt in range(MaxQueueAmount):
    workQueue.put("q.{}".format(cnt))
# 开启新线程
for t in threads:
    t.start()
# 等待队列清空
while not workQueue.empty():
    pass
# 通知线程退出
exitFlag = 1
# 等待所有线程完成
for t in threads:
    t.join()
print("Main process has exited!")
