
'''
if <condition>:
	<statements>
elif <condition>:
	<statements>
else:
	<statements>
'''
#age = float(input("Please enter your dog's age:"))
age = 5
if age < 0:
	print("Are you kidding me?")
elif age <= 1:
	human = age * 14
elif age <= 2:
	human = 14 + (age - 1) * (22 - 14)
else:
	human = 22 + (age - 2) * 5
print("The equivalent of human's age: %.2f \n" % (human))

'''
while <condition>:
	<statements>
else:
	<statements>
'''
#num = int(input("Please enter a integer:"))
num = 100
cnt, sum = 1, 0
while cnt <= num:
	sum += cnt
	cnt += 1
print("1 to %d: %d \n" % (num, sum))

'''
for <variable> in <sequence>:
	<statements>
else:
	<statements>
'''
sites = ["Baidu", "Google", "HaydnLiao", "Yahoo"]
#sites = []
for site in sites:
	if site == "HaydnLiao":
		print("HaydnLiao.win")
		break
	print("site:", site)
else:
	print("Nothing!")
print("Done! \n")

for i in range(len(sites)):
	print(i, sites[i])
print()

# break, continue, pass
# pass语句为空语句,是为了保持程序结构的完整性
while True:
	pass # 等待键盘中断(Ctrl + C)

