# -*- coding:utf-8 -*-
# 正则表达式是一个特殊的字符序列，检查一个字符串是否与某种模式匹配。
# re 模块使 Python 语言拥有全部的正则表达式功能。
# compile 函数根据一个模式字符串和可选的标志参数生成一个正则表达式对象。
# 该对象拥有一系列方法用于正则表达式匹配和替换。

# re.match(pattern, string, flags=0)
# 尝试从字符串的起始位置匹配一个模式，如果不是起始位置匹配成功的话，返回none。
import re
print(re.match("www", "www.baidu.com"))	# 在起始位置匹配
print(re.match("com", "www.baidu.com"))	# 不在起始位置匹配
print()

# 使用group(num) 或 groups() 匹配对象函数来获取匹配表达式。
# group(num=0)	匹配的整个表达式的字符串。
# group() 可以一次输入多个组号，在这种情况下它将返回一个包含那些组所对应值的元组。
# groups()	返回一个包含所有小组字符串的元组，从 1 到 n 所含的小组号。
import re
line = "Cats are smarter than dogs"
matchObj = re.match( r'(.*) are (.*?) .*', line, re.M | re.I)
if matchObj:
	print ("matchObj.group(): ", matchObj.group())
	print ("matchObj.group(1):", matchObj.group(1))
	print ("matchObj.group(2):", matchObj.group(2))
else:
	print ("No match!")
print()

# re.search(pattern, string, flags=0) 扫描整个字符串并返回第一个成功的匹配。
import re
print(re.search('www', 'www.runoob.com'))	# 在起始位置匹配
print(re.search('com', 'www.runoob.com'))	# 不在起始位置匹配
print()

# re.match与re.search的区别
# re.match只匹配字符串的开始，如果字符串开始不符合正则表达式，则匹配失败，函数返回None;
# re.search匹配整个字符串，直到找到一个匹配。

# re.sub(pattern, repl, string, count=0)用于替换字符串中的匹配项。
'''
pattern : 正则中的模式字符串。
repl : 替换的字符串，也可为一个函数。
string : 要被查找替换的原始字符串。
count : 模式匹配后替换的最大次数，默认 0 表示替换所有的匹配。
'''
import re
phone = "2004-959-559 # 这是一个电话号码"
print(phone)
num = re.sub(r'#.*$', "", phone)# 删除注释
print("phone number:", num)
num = re.sub(r'\D', "", phone)	# 移除非数字的内容
print ("phone number:", num)
print()
# 替换参数repl也可以为一个返回字符串的函数。
def double(matchObj):
	value = int(matchObj.group('value'))
	# print(matchObj.group())
	return str(value*2)
s = 'A23G4HFD567'
print(s)
print(re.sub('(?P<value>\d+)', double, s))

# 可选标志
# re.I(忽略大小写)、re.L(依赖locale)、re.M(多行模式)
# re.S(.匹配所有字符)、re.U(依赖Unicode)、re.X(详细模式)
'''
re.I	使匹配对大小写不敏感
re.L	做本地化识别（locale-aware）匹配
re.M	多行匹配，影响 ^ 和 $
re.S	使 . 匹配包括换行在内的所有字符
re.U	根据Unicode字符集解析字符。这个标志影响 \w, \W, \b, \B.
re.X	该标志通过给予你更灵活的格式以便你将正则表达式写得更易于理解。
'''

# 正则表达式模式
'''
^	匹配字符串的开头。
$	匹配字符串的末尾。
.	匹配除 "\n" 之外的任何单个字符。要匹配包括 '\n' 在内的任何字符，请使用象 '[.\n]' 的模式。
	当re.DOTALL标记被指定时，则可以匹配包括换行符的任意字符。
[...]	用来表示一组字符,单独列出：[amk] 匹配 'a'，'m'或'k'
[^...]	不在[]中的字符：[^abc] 匹配除了a,b,c之外的字符。
re*	匹配0个或多个的表达式。
re+	匹配1个或多个的表达式。
re?	匹配0个或1个由前面的正则表达式定义的片段，非贪婪方式。
re{ n}，re{ n,}	精确匹配n个前面表达式。
re{ n, m}	匹配 n 到 m 次由前面的正则表达式定义的片段，贪婪方式。
a | b	匹配a或b。
(re)	匹配括号内的表达式，也表示一个组。
(?imx)	正则表达式包含三种可选标志：i, m, 或 x 。只影响括号中的区域。
(?-imx)	正则表达式关闭 i, m, 或 x 可选标志。只影响括号中的区域。
(?: re)	类似 (...), 但是不表示一个组。
(?imx: re)	在括号中使用i, m, 或 x 可选标志。
(?-imx: re)	在括号中不使用i, m, 或 x 可选标志。
(?#...)	注释.
(?= re)	前向肯定界定符。如果所含正则表达式，以 ... 表示，在当前位置成功匹配时成功，否则失败。
		但一旦所含表达式已经尝试，匹配引擎根本没有提高；模式的剩余部分还要尝试界定符的右边。
(?! re)	前向否定界定符。与肯定界定符相反；当所含表达式不能在字符串当前位置匹配时成功。
(?> re)	匹配的独立模式，省去回溯。
\w	匹配字母数字。等价于[A-Za-z0-9_].
\W	匹配非字母数字。等价于[^A-Za-z0-9_]。
\s	匹配任意空白字符，等价于 [\t\n\r\f\v].
\S	匹配任意非空字符。等价于 [^ \f\n\r\t\v]。
\d	匹配任意数字，等价于 [0-9].
\D	匹配任意非数字。等价于 [^0-9]。
\A	匹配字符串开始。
\Z	匹配字符串结束，如果是存在换行，只匹配到换行前的结束字符串。
\z	匹配字符串结束。
\G	匹配最后匹配完成的位置。
\b	匹配一个单词边界，也就是指单词和空格间的位置。例如， 'er\b' 可以匹配"never" 中的 'er'，但不能匹配 "verb" 中的 'er'。
\B	匹配非单词边界。'er\B' 能匹配 "verb" 中的 'er'，但不能匹配 "never" 中的 'er'。
\n, \t, 等.	匹配一个换行符。匹配一个制表符。等
\1...\9	匹配第n个分组的内容。
\10	匹配第n个分组的内容，如果它经匹配。否则指的是八进制字符码的表达式。
'''
str = 'pm1.2345,tp23.456,hm45.678'
sss = re.finditer('([a-z]{2}\d+(\.\d+)?)', str)
print sss
for ss in sss:
	print ss.group()
sss = re.findall('([a-z]{2}\d+(\.\d+)?)', str)
print sss
for ss in sss:
	print ss[0]
