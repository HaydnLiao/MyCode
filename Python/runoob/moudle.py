
import sys 
print("Command lines parameter:")
for i in sys.argv:			# 包含命令行参数的列表
	print(' ', i)
print("Python's path:")		# 解释器自动查找所需模块的路径的列表
for i in sys.path:
	print(' ', i)
print()

# 模块除了方法定义,还可以包括可执行的代码.这些代码一般用来初始化这个模块,只有在第一次被导入时才会被执行.
# 每个模块都有一个__name__属性,当其值是'__main__'时,表明该模块自身在运行,否则是被引入.
import moudle_test			# 定义了名称moudle_test
moudle_test.printArgv()
from moudle_test import printArgv	# 定义了名称printArgv
printArgv()
from moudle_test import *	# 应不用
printArgv()
print()


# 内置的函数 dir() 可以找到模块内定义的所有名称,以一个字符串列表的形式返回.
print("moudle_test:")
for i in dir(moudle_test):
	print(' ', i)
print()

# 如果没有给定参数,那么 dir() 函数会罗列出当前定义的所有名称.
print("moudle:")
for i in dir():
	print(' ', i)

# 特别的模块 sys 内置在每一个 Python 解析器中.变量 sys.ps1 和 sys.ps2 定义了主提示符和副提示符所对应的字符串.

