
# 日期和时间
# time 和 calendar 模块可以用于格式化日期和时间。
# 时间间隔是以秒为单位的浮点小数。

#获取时间戳
# 函数time.time()用于获取当前时间戳.时间戳单位最适于做日期运算。
# 每个时间戳都以自从1970年1月1日午夜（历元）经过了多长时间来表示。
# 1970年之前的日期就无法以此表示了，之后的UNIX和Windows只支持到2038年。
import time
ticks = time.time()
print("time.time():", type(ticks), ticks)
print()

'''
时间元组 struct_time
0   tm_year	    2008         4位数年
1	tm_mon  	1 到 12      月
2	tm_mday	    1 到 31      日
3	tm_hour	    0 到 23      小时
4	tm_min	    0 到 59      秒
5	tm_sec	    0 到 61 (60或61 是闰秒)
6	tm_wday	    0 到 6 (0是周一)
7	tm_yday	    一年中的第几天，1 到 366
8	tm_isdst    是否为夏令时，1(夏令时)、0(不是夏令时)、-1(未知)，默认 -1
'''
# 接收时间戳，返回当地时间下的时间元组
# time.tm_isdst可取0或1，取决于当地当时是不是夏令时。
# import time
localtime = time.localtime()
print("time.localtime():", type(localtime), localtime)
# 接收时间戳，返回返回格林威治天文时间元组
gtime = time.gmtime()
print("time.gmtime()", type(gtime), gtime)
# 接收时间元组返回时间戳
mtime = time.mktime(localtime)
ktime = time.mktime(gtime)
print("time.mktime(xxx):", type(mtime), mtime, type(ktime), ktime)
print()

# 获取格式化的时间
# 接受时间元组并返回一个可读的形式为"Tue Dec 11 18:07:14 2008"的24个字符的字符串。
# import time 
localtime = time.asctime()
print("time.asctime():", type(localtime), localtime)
localtime = time.ctime()
print("time.ctime():", type(localtime), localtime)
# 自定义格式化时间
ftime = time.strftime("%Y-%m-%d %a %X", time.localtime())
print("time.strftime:", type(ftime), ftime)
print()
'''
时间日期格式化符号：
%y 两位数的年份表示（00-99）
%Y 四位数的年份表示（000-9999）
%m 月份（01-12）
%d 月内中的一天（0-31）
%H 24小时制小时数（0-23）
%I 12小时制小时数（01-12）
%M 分钟数（00=59）
%S 秒（00-59）
%a 本地简化星期名称
%A 本地完整星期名称
%b 本地简化的月份名称
%B 本地完整的月份名称
%c 本地相应的日期表示和时间表示
%j 年内的一天（001-366）
%p 本地A.M.或P.M.的等价符
%U 一年中的星期数（00-53）星期天为星期的开始
%w 星期（0-6），星期天为星期的开始
%W 一年中的星期数（00-53）星期一为星期的开始
%x 本地相应的日期表示
%X 本地相应的时间表示
%Z 当前时区的名称
%% %号本身
'''

# time.altzone 返回格林威治西部的夏令时地区的偏移秒数。
# 如果该地区在格林威治东部会返回负值（如西欧，包括英国）。
# 对夏令时启用地区才能使用。
print("time.altzone:", type(time.altzone), time.altzone)
# 当地时区（未启动夏令时）距离格林威治的偏移秒数.
# >0，美洲; <=0大部分欧洲，亚洲，非洲。
print("time.timezone:", type(time.timezone), time.timezone)
# 返回带夏令时的本地时区名称和不带的。
print("time.tzname:", type(time.tzname), time.tzname[0].encode('latin-1').decode('gbk'))
print()

# time.clock() 以浮点数计算的秒数返回当前的CPU时间,用来衡量不同程序的耗时.
# 在UNIX系统上，它返回的是"进程时间"，它是用秒表示的浮点数（时间戳）。
# 而在WINDOWS中，第一次调用，返回的是进程运行的实际时间。
# 而第二次之后的调用是自第一次调用以后到现在的运行时间。
print("time.clock()-1:", time.clock())
# time.sleep(secs) 推迟调用线程的运行，secs指秒数。
print("time.sleep(secs):", time.sleep(2))
print("time.clock()-2:", time.clock())
print()

# 获取字符日历 calendar
# 星期一是默认的每周第一天，星期天是默认的最后一天。
# 更改设置需调用calendar.setfirstweekday()函数。
'''
1	calendar.calendar(year,w=2,l=1,c=6)
返回一个多行字符串格式的year年年历，3个月一行，间隔距离为c。每日宽度间隔为w字符。每行长度为21* W+18+2* C。l是每星期行数。
2	calendar.firstweekday( )
返回当前每周起始日期的设置。默认情况下，首次载入caendar模块时返回0，即星期一。
3	calendar.isleap(year)                   是闰年返回True，否则为false。
4	calendar.leapdays(y1,y2)                返回在Y1，Y2两年之间的闰年总数。
5	calendar.month(year,month,w=2,l=1)
返回一个多行字符串格式的year年month月日历，两行标题，一周一行。每日宽度间隔为w字符。每行的长度为7*w+6。l是每星期的行数。
6	calendar.monthcalendar(year,month)
返回一个整数的单层嵌套列表。每个子列表装载代表一个星期的整数。Year年month月外的日期都设为0;范围内的日子都由该月第几日表示，从1开始。
7	calendar.monthrange(year,month)
返回两个整数。第一个是该月的星期几的日期码，第二个是该月的日期码。日从0（星期一）到6（星期日）;月从1到12。
8	calendar.prcal(year,w=2,l=1,c=6)        相当于 print calendar.calendar(year,w,l,c).
9	calendar.prmonth(year,month,w=2,l=1)    相当于 print calendar.calendar（year，w，l，c）。
10	calendar.setfirstweekday(weekday)       设置每周的起始日期码。0（星期一）到6（星期日）。
11	calendar.timegm(tupletime)
和time.gmtime相反：接受一个时间元组形式，返回该时刻的时间辍（1970纪元后经过的浮点秒数）。
12	calendar.weekday(year,month,day)
返回给定日期的日期码。0（星期一）到6（星期日）。月份为 1（一月） 到 12（12月）。
'''
import calendar
cal = calendar.month(2017, 7, 3, 1)
print (cal)

