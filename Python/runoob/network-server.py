
import socket
import sys
# 创建 socket 对象
serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
# 获取本地主机名
hostName = socket.gethostname()
# hostName = "192.168.235.1"
# 设置端口号
port = 12345

# 绑定端口
serverSocket.bind((hostName, port))
# 设置最大连接数，超过后排队
serverSocket.listen(5)
while True:
    # 建立客户端连接
    clientsocket, addr = serverSocket.accept()
    print("connect address: {}:{}" .format(addr[0], addr[1]))
    msg="Send from {0}!\r\n".format(hostName)
    clientsocket.send(msg.encode('utf-8'))
    clientsocket.close()
