
# 输出-表达式语句输出
# 输出-print()
# 输出-文件-write()
# 输出-标准输出文件-sys.stdout()

'''
转换成字符串
	str()	函数返回一个用户易读的表达形式
	repr()	产生一个解释器易读的表达形式
'''

# str.format() 
print('{} and {}'.format('Google', 'Baidu'))
print('{0} and {1}'.format('Google', 'Baidu'))
print('{1} and {0}'.format('Google', 'Baidu'))
print('{name} website: {site}'.format(name='Baidu', site='www.baidu.com'))
print('{0}, {1} and {other}'.format('Google', 'Baidu', other='Yahoo'))
# '!a' (使用 ascii()), '!s' (使用 str()) 和 '!r' (使用 repr()) 可以用于在格式化某个值之前对其进行转化.
print('{!a}'.format('c'))
# 可选项 ':' 和格式标识符可以跟着字段名.
import math
print('pi:{0:.3f}'.format(math.pi))
# 在 ':' 后传入一个整数, 可以保证该域至少有这么多的宽度.
table = {'Google': 1, 'Baidu': 2, 'Yahoo': 3}
for name, number in table.items():
	print('{0:8} ==> {1:5d}'.format(name, number))
print('Yahoo: {0[Yahoo]:d}; Google: {0[Google]:d}; Baidu: {0[Baidu]:d}'.format(table))
print('Yahoo: {Yahoo:d}; Google: {Google:d}; Baidu: {Baidu:d}'.format(**table))
print()

# 旧式字符串格式化: %操作符也可以实现字符串格式化.
# 旧式的格式化最终会从该语言中移除,应该更多的使用 str.format().

# 输入-input()
# input()	从标准输入读入一行文本,默认的标准输入是键盘

# 读/写文件-open()、write()、close()
'''
open(filename, mode)
	返回一个 file 对象
	filename:要访问的文件名称的字符串值
	mode:文件打开模式,这个参数是非强制的,默认文件访问模式为只读(r)
不同模式打开文件的完全列表:
	r	以只读方式打开文件.文件的指针将会放在文件的开头.这是默认模式.
	rb	以二进制格式打开一个文件用于只读.文件指针将会放在文件的开头.这是默认模式.
	r+	打开一个文件用于读写.文件指针将会放在文件的开头.
	rb+	以二进制格式打开一个文件用于读写.文件指针将会放在文件的开头.
	w	打开一个文件只用于写入.如果该文件已存在则将其覆盖.如果该文件不存在,创建新文件.
	wb	以二进制格式打开一个文件只用于写入.如果该文件已存在则将其覆盖.如果该文件不存在,创建新文件.
	w+	打开一个文件用于读写.如果该文件已存在则将其覆盖.如果该文件不存在,创建新文件.
	wb+	以二进制格式打开一个文件用于读写.如果该文件已存在则将其覆盖.如果该文件不存在,创建新文件.
	a	打开一个文件用于追加.如果该文件已存在,文件指针将会放在文件的结尾.如果该文件不存在,创建新文件进行写入.
	ab	以二进制格式打开一个文件用于追加.如果该文件已存在,文件指针将会放在文件的结尾.如果该文件不存在,创建新文件进行写入.
	a+	打开一个文件用于读写.如果该文件已存在,文件指针将会放在文件的结尾.文件打开时会是追加模式.如果该文件不存在,创建新文件用于读写.
	ab+	以二进制格式打开一个文件用于追加.如果该文件已存在,文件指针将会放在文件的结尾.如果该文件不存在,创建新文件用于读写.
'''

# 文件对象的方法
# f.read(size)读取一定数目的数据,作为字符串或字节对象返回.当 size 被忽略了或者为负, 那么该文件的所有内容都将被读取并且返回.
#f = open("python-test-read.txt", 'rb')
f = open("python-test-read.txt", 'r', encoding = 'utf-8')
s = f.read()
print('read()\n' + s)
f.close()
print()

# f.readline() 会从文件中读取单独的一行.换行符为 '\n'.如果返回一个空字符串, 说明已经已经读取到最后一行.
f = open("python-test-read.txt", 'r', encoding = 'utf-8')
s = f.readline()
print('readline()\n' + s)
f.close()
print()

# f.readlines() 返回该文件中包含的所有行.若给定sizeint>0,返回总和大约为sizeint字节的行, 实际读取值可能比 sizeint 较大, 因为需要填充缓冲区.
f = open("python-test-read.txt", 'r', encoding = 'utf-8')
s = f.readlines()
print('readlines()\n' + str(s))
f.close()
print()

# 迭代文件对象
f = open("python-test-read.txt", 'r', encoding = 'utf-8')
print('iterator')
for line in f:
	print(line, end='')
f.close()
print()

# f.write(string) 将 string 写入到文件中, 然后返回写入的字符数.
f = open("python-test-write.txt", 'w')
num = f.write("python-test-write.txt")
print("wrote number:", num)
f.close()
print()

# f.tell() 返回文件对象当前所处的位置, 它是从文件开头开始算起的字节数.
f = open("python-test-read.txt", 'r', encoding = 'utf-8')
s = f.readline()
print("f.tell()", f.tell())
print()

# f.seek(offset, from_what) 改变文件当前的位置.from_what, 0表示开头, 1表示当前位置, 2表示文件的结尾, 默认为0.
# 非二进制的文本文件,只能相对于文件起始位置进行定位.
# f.tell() 返回文件对象当前所处的位置, 它是从文件开头开始算起的字节数.
f = open("python-test-read.txt", 'rb')
print("f.tell():", f.tell())
print("f.seek(12):", f.seek(12))
print("f.tell():", f.tell())
print("f.seek(12, 1):", f.seek(12, 1))
print("f.tell():", f.tell())
print("f.seek(-12, 2):", f.seek(-12, 2))
print("f.tell():", f.tell())
print()

# f.close() 用来关闭文件并释放系统的资源.
# 当处理一个文件对象时, 使用 with 关键字是非常好的方式.在结束后, 它会帮你正确的关闭文件.

with open("python-test-read.txt", 'rb') as f:
	read_data = f.read()
print(read_data)
print("f.closed:", f.closed)
print()

# pickle模块实现了基本的数据序列和反序列化.
# 通过pickle模块的序列化操作我们能够将程序中运行的对象信息保存到文件中去,永久存储.
# 通过pickle模块的反序列化操作,我们能够从文件中创建上一次程序保存的对象.

# 写文件-pickle.dump(obj, file, [,protocol]) 
# protocol 0:ASCII协议,所序列化的对象使用可打印的ASCII码表示;1:老式的二进制协议;2:2.3版本引入的新二进制协议,较以前的更高效.其中协议0和1兼容老版本的python.protocol默认值为0.
# file 一个以'w'方式打开的文件或者一个StringIO对象或者其他任何实现write()接口的对象.如果protocol>=1,文件对象需要是二进制模式打开的.
import pickle
dictT = {'a': [1, 2.0, 3, 4+6j],
		'b': ('string', u'Unicode string'),
		'c': None}
listT = [1, 2, 3]*2
output = open('data.pkl', 'wb')
pickle.dump(dictT, output)
pickle.dump(listT, output)
print(dictT)
print(listT)
output.close()
# 读文件-pickle.load(file)
# 从 file 中读取一个字符串,并将它重构为原来的python对象.
input = open('data.pkl', 'rb')
dictTT = pickle.load(input)
listTT = pickle.load(input)
print(dictTT)
print(listTT)
input.close()
print 

'''
file对象其他常用函数
	file.flush()	刷新文件内部缓冲,直接把内部缓冲区的数据立刻写入文件, 而不是被动的等待输出缓冲区写入.
	file.fileno()	返回一个整型的文件描述符(file descriptor FD 整型), 可以用在如os模块的read方法等一些底层操作上.
	file.isatty()	如果文件连接到一个终端设备返回 True,否则返回 False.
	file.truncate([size])	从文件的首行首字符开始截断,截断文件为 size 个字符,无 size 表示从当前位置截断.其中 Widnows 系统下的换行代表2个字符大小.
	file.writelines(sequence)	向文件写入一个序列字符串列表,如果需要换行则要自己加入每行的换行符.
'''
