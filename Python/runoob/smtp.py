
# smtplib模块用于发送电子邮件。
import smtplib
from email.mime.text import MIMEText
mail_from = "HaydnLiao@163.com"
# mail_to = "HaydnLiao@qq.com"
mail_to = "HaydnLiao@163.com"
mail_user = "HaydnLiao@163.com"
mail_pass = "xxx"
'''
#发送普通邮件
content = "python smtplib moudle sends this e-mail!"
msg = MIMEText(content, 'plain')
msg['From'] = mail_from
msg['To'] = mail_to
msg['Subject'] = "python smtplib test"
print(msg.as_string())
print('-'*20)
try:
	server = smtplib.SMTP("smtp.163.com", 25)
	server.login(mail_user, mail_pass)
	server.sendmail(mail_from, mail_to, msg.as_string())
except Exception as e:
	print("<Exception>", e)
finally:
	rtn = server.quit()
	print("server.quit():", rtn)
print()
'''
'''
# 发送HTML格式的邮件
content = """
<html>

<head>
<style>
body {
	margin: 0;
	padding: 0;
	font: 12px/1.5 arial;
	text-align:center;
	height:100%;
	width:100%;
}
img {
	max-width: 100%;
	max-height: 100%;
	margin:2px;
	border-radius: 6px;
}
a {
	text-decoration:none;
}
#header {
    background-color:black;
    color:white;
	margin:2% 0 0 0;
    padding:5px;
}
#section {
    width:80%;
	margin:0 auto;
    padding:10px;
}
#footer {
    background-color:black;
	font-size:medium;
    color:white;
    clear:both;
    padding:5px;
	position: fixed;
	bottom:0;
	min-width:100%;
}
</style>
</head>

<body>
<div id="header">
	<h1>HAYDNLIAO.WIN</h1>
</div>
<div id="section">
	<p>
		<img src="http://haydnliao.win/HaydnLiao.png" width="440" height="440"/>
	</p>
	<h2>HaydnLiao</h2>
	<span>Dongguan University of Technology</span>
	<p>
		<a href="https://github.com/HaydnLiao">
			<img src="http://haydnliao.win/github.png" width="38" height="38"/>
		</a>
		<a href="http://blog.csdn.net/hxiaohai">
			<img src="http://haydnliao.win/csdn.png" width="38" height="38"/>
		</a>
		<a href="https://www.linkedin.com/in/haydnliao">
			<img src="http://haydnliao.win/linkedin.png" width="38" height="38"/>
		</a>
	</p>
</div>
<br/><br/><br/>
<div id="footer">
	© 2017.01.01 HaydnLiao</a>
</div>
</body>

</html>

"""
msg = MIMEText(content, 'html')
msg['From'] = mail_from
msg['To'] = mail_to
msg['Subject'] = "python smtplib test"
print(msg.as_string())
print('-'*20)
try:
	server = smtplib.SMTP("smtp.163.com", 25)
	server.login(mail_user, mail_pass)
	server.sendmail(mail_from, mail_to, msg.as_string())
except Exception as e:
	print("<Exception>", e)
finally:
	rtn = server.quit()
	print("server.quit():", rtn)
print()
'''
'''
# 发送带附件的邮件
from email.mime.multipart import MIMEMultipart
# 创建一个带附件的实例
msg = MIMEMultipart()
msg['From'] = mail_from
msg['To'] = mail_to
msg['Subject'] = "python smtplib test"
# 邮件正文内容
msg.attach(MIMEText("python smtplib moudle sends this e-mail!", 'plain'))
# 构造附件一
att1 = MIMEText(open('./smtp.py', 'rb').read(), 'base64', 'utf-8')
att1["Content-Type"] = 'application/octet-stream'
att1["Content-Disposition"] = 'attachment; filename="stmplib_smtp.py"' # filename 可任意写
msg.attach(att1)
# 构造附件二
att2 = MIMEText(open('./standard-lib.py', 'rb').read(), 'base64', 'utf-8')
att2["Content-Type"] = 'application/octet-stream'
att2["Content-Disposition"] = 'attachment; filename="standard-lib.py"' # filename 可任意写
msg.attach(att2)
print(msg.as_string())
print('-'*20)
try:
	server = smtplib.SMTP("smtp.163.com", 25)
	server.login(mail_user, mail_pass)
	server.sendmail(mail_from, mail_to, msg.as_string())
except Exception as e:
	print("<Exception>", e)
finally:
	rtn = server.quit()
	print("server.quit():", rtn)
print()
'''

# 在 HTML 文本中添加图片
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
# 创建一个带附件的实例
msgRoot = MIMEMultipart()
msgRoot['From'] = mail_from
msgRoot['To'] = mail_to
msgRoot['Subject'] = "python smtplib test"
# 邮件正文内容
content = """
<p>python smtplib moudle sends this e-mail!</p>
<p><img src="cid:image1"></p>
"""
msgRoot.attach(MIMEText(content, 'html'))
# 指定图片
fp = open('./HaydnLiao.png', 'rb')
msgImg = MIMEImage(fp.read())
fp.close()
# 定义图片 ID，在 HTML 文本中引用
msgImg.add_header('Content-ID', '<image1>')
msgRoot.attach(msgImg)
print('-'*20)
try:
	server = smtplib.SMTP("smtp.163.com", 25)
	server.login(mail_user, mail_pass)
	server.sendmail(mail_from, mail_to, msgRoot.as_string())
except Exception as e:
	print("<Exception>", e)
finally:
	rtn = server.quit()
	print("server.quit():", rtn)
print()

