
# -*- coding: UTF-8 -*-

import socket
import sys
# 创建 socket 对象
clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# 获取本地主机名
# hostName = socket.gethostname() 
hostName = "192.168.235.1"
# 设置端口号
port = 12345

# 连接服务，指定主机和端口
clientSocket.connect((hostName, port))
# 接收小于 1024 字节的数据
msg = clientSocket.recv(1024)
clientSocket.close()
print (msg.decode('utf-8'))
