# -*- coding: utf-8 -*-
# filename: main.py

import web
from handle import Handle, getAjaxFileName
import threading
from nbiot_server import nbiot_server

urls = (
    '/([.\w]*)?', 'Handle',
    # '/favicon.ico', 'Icon'
)

if __name__ == '__main__':
    threading.Thread(target=nbiot_server, args=(getAjaxFileName(),)).start()
    app = web.application(urls, globals())
    app.run()

